﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视点人员权限变更信息
	/// </summary>
	[Serializable]
	public partial class Sys_Info
	{
		public Sys_Info()
		{}
		#region Model
		private string _gcmc;
		private string _zbgs;
		private int? _kqgrdh;
		private string _bflj;
		private string _bfsj;
		/// <summary>
		/// 
		/// </summary>
		public string gcmc
		{
			set{ _gcmc=value;}
			get{return _gcmc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zbgs
		{
			set{ _zbgs=value;}
			get{return _zbgs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? kqgrdh
		{
			set{ _kqgrdh=value;}
			get{return _kqgrdh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bflj
		{
			set{ _bflj=value;}
			get{return _bflj;}
		}
		/// <summary>
		/// 特殊格式：*MHH:ss每月几号什么时候  *WHH:ss每周几什么时候 DHH:ss每天什么时候
		/// </summary>
		public string bfsj
		{
			set{ _bfsj=value;}
			get{return _bfsj;}
		}
		#endregion Model

	}
}

