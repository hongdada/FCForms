﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 数据库的版本维护
	/// </summary>
	[Serializable]
	public partial class DBVer
	{
		public DBVer()
		{}
		#region Model
		private string _verid;
		private string _softverid;
		/// <summary>
		/// 
		/// </summary>
		public string VerId
		{
			set{ _verid=value;}
			get{return _verid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SoftVerId
		{
			set{ _softverid=value;}
			get{return _softverid;}
		}
		#endregion Model

	}
}

