﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视点人员权限变更信息
	/// </summary>
	[Serializable]
	public partial class Upload
	{
		public Upload()
		{}
		#region Model
		private DateTime _uploaddate;
		private long _uploadlsh;
		/// <summary>
		/// 
		/// </summary>
		public DateTime UploadDate
		{
			set{ _uploaddate=value;}
			get{return _uploaddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public long Uploadlsh
		{
			set{ _uploadlsh=value;}
			get{return _uploadlsh;}
		}
		#endregion Model

	}
}

