﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 数据库的版本维护
	/// </summary>
	[Serializable]
	public partial class Fenb_Info
	{
		public Fenb_Info()
		{}
		#region Model
		private string _fbid;
		private string _fbfid;
		private string _fbmc;
		private string _bz;
		/// <summary>
		/// 
		/// </summary>
		public string fbid
		{
			set{ _fbid=value;}
			get{return _fbid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fbfid
		{
			set{ _fbfid=value;}
			get{return _fbfid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fbmc
		{
			set{ _fbmc=value;}
			get{return _fbmc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bz
		{
			set{ _bz=value;}
			get{return _bz;}
		}
		#endregion Model

	}
}

