﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 考勤记录信息(门禁里的记录信息)
	/// </summary>
	[Serializable]
	public partial class Plugin_CSCECTWO_Record
	{
		public Plugin_CSCECTWO_Record()
		{}
		#region Model
		private string _sfzh;
		private DateTime? _sjjysj;
		private DateTime? _jccnssj;
		private DateTime? _tccnssj;
		private DateTime? _ldhtssj;
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? sjjysj
		{
			set{ _sjjysj=value;}
			get{return _sjjysj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? jccnssj
		{
			set{ _jccnssj=value;}
			get{return _jccnssj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? tccnssj
		{
			set{ _tccnssj=value;}
			get{return _tccnssj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ldhtssj
		{
			set{ _ldhtssj=value;}
			get{return _ldhtssj;}
		}
		#endregion Model

	}
}

