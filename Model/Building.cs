using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 实体类Building 。(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	public class Building
	{
		public Building()
		{}
		#region Model
		private int _buildingid;
		private string _buildingname;
		/// <summary>
		/// 
		/// </summary>
		public int BuildingID
		{
			set{ _buildingid=value;}
			get{return _buildingid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildingName
		{
			set{ _buildingname=value;}
			get{return _buildingname;}
		}
		#endregion Model

	}
}

