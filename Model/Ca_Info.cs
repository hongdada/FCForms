﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 记录所有卡信息
	/// </summary>
	[Serializable]
	public partial class Ca_Info
	{
		public Ca_Info()
		{}
		#region Model
		private string _kh;
		private string _kbs;
		private int? _klx;
		private bool _sfyx;
		/// <summary>
		/// 卡CSN号
		/// </summary>
		public string kh
		{
			set{ _kh=value;}
			get{return _kh;}
		}
		/// <summary>
		/// 卡片标识号
		/// </summary>
		public string kbs
		{
			set{ _kbs=value;}
			get{return _kbs;}
		}
		/// <summary>
		/// 0:临时卡 1：务工卡 2：车卡
		/// </summary>
		public int? klx
		{
			set{ _klx=value;}
			get{return _klx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sfyx
		{
			set{ _sfyx=value;}
			get{return _sfyx;}
		}
		#endregion Model

	}
}

