﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视点人员权限变更信息
	/// </summary>
	[Serializable]
	public partial class User_Info
	{
		public User_Info()
		{}
		#region Model
		private string _username;
		private string _password;
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PassWord
		{
			set{ _password=value;}
			get{return _password;}
		}
		#endregion Model

	}
}

