﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 人员排班信息
	/// </summary>
	[Serializable]
	public partial class RyPrint_Info
	{
		public RyPrint_Info()
		{}
		#region Model
		private string _sfzh;
		private string _zw1;
		private string _zw2;
		private string _zw3;
		private string _zw4;
		private string _zw5;
		private string _zw6;
		private string _zw7;
		private string _zw8;
		private string _zw9;
		private string _zw10;
		private bool _gly;
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw1
		{
			set{ _zw1=value;}
			get{return _zw1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw2
		{
			set{ _zw2=value;}
			get{return _zw2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw3
		{
			set{ _zw3=value;}
			get{return _zw3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw4
		{
			set{ _zw4=value;}
			get{return _zw4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw5
		{
			set{ _zw5=value;}
			get{return _zw5;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw6
		{
			set{ _zw6=value;}
			get{return _zw6;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw7
		{
			set{ _zw7=value;}
			get{return _zw7;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw8
		{
			set{ _zw8=value;}
			get{return _zw8;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw9
		{
			set{ _zw9=value;}
			get{return _zw9;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zw10
		{
			set{ _zw10=value;}
			get{return _zw10;}
		}
		/// <summary>
		/// 1:管理员 0：普通用户
		/// </summary>
		public bool gly
		{
			set{ _gly=value;}
			get{return _gly;}
		}
		#endregion Model

	}
}

