﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视地点信息
	/// </summary>
	[Serializable]
	public partial class Jsd_Info
	{
		public Jsd_Info()
		{}
		#region Model
		private string _jsdbh;
		private string _jsdfbh;
		private string _jsdm;
		private string _bz;
		private int? _lx;
		/// <summary>
		/// 监视点编号
		/// </summary>
		public string jsdbh
		{
			set{ _jsdbh=value;}
			get{return _jsdbh;}
		}
		/// <summary>
		/// 监视点父编号
		/// </summary>
		public string jsdfbh
		{
			set{ _jsdfbh=value;}
			get{return _jsdfbh;}
		}
		/// <summary>
		/// 监视点名
		/// </summary>
		public string jsdm
		{
			set{ _jsdm=value;}
			get{return _jsdm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bz
		{
			set{ _bz=value;}
			get{return _bz;}
		}
		/// <summary>
		/// 0:区域 1:门
		/// </summary>
		public int? lx
		{
			set{ _lx=value;}
			get{return _lx;}
		}
		#endregion Model

	}
}

