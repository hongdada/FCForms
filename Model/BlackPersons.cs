﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// BlackPersons:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class BlackPersons
	{
		public BlackPersons()
		{}
		#region Model
		private int _id;
		private string _name;
		private string _sfzh;
		private bool _sex;
		private string _remark;
		private DateTime _createtime;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SFZH
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime createtime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		#endregion Model

	}
}

