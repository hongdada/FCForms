﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 记录所有卡信息
	/// </summary>
	[Serializable]
	public partial class Compute_Result
	{
		public Compute_Result()
		{}
		#region Model
		private long? _yjskqjllsh=0;
		private bool _jszt= false;
		/// <summary>
		/// 
		/// </summary>
		public long? yjskqjllsh
		{
			set{ _yjskqjllsh=value;}
			get{return _yjskqjllsh;}
		}
		/// <summary>
		/// 0:失败 1：成功
		/// </summary>
		public bool jszt
		{
			set{ _jszt=value;}
			get{return _jszt;}
		}
		#endregion Model

	}
}

