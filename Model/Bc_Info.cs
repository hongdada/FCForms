﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// Bc_Info:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Bc_Info
	{
		public Bc_Info()
		{}
		#region Model
		private int _bcid;
		private string _bcm;
		private string _sjd1ks;
		private string _sjd1js;
		private string _sjd2ks;
		private string _sjd2js;
		private string _sjd3ks;
		private string _sjd3js;
		private string _sjd4ks;
		private string _sjd4js;
		/// <summary>
		/// 
		/// </summary>
		public int bcid
		{
			set{ _bcid=value;}
			get{return _bcid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bcm
		{
			set{ _bcm=value;}
			get{return _bcm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd1ks
		{
			set{ _sjd1ks=value;}
			get{return _sjd1ks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd1js
		{
			set{ _sjd1js=value;}
			get{return _sjd1js;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd2ks
		{
			set{ _sjd2ks=value;}
			get{return _sjd2ks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd2js
		{
			set{ _sjd2js=value;}
			get{return _sjd2js;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd3ks
		{
			set{ _sjd3ks=value;}
			get{return _sjd3ks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd3js
		{
			set{ _sjd3js=value;}
			get{return _sjd3js;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd4ks
		{
			set{ _sjd4ks=value;}
			get{return _sjd4ks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sjd4js
		{
			set{ _sjd4js=value;}
			get{return _sjd4js;}
		}
		#endregion Model

	}
}

