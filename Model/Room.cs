using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 实体类Room 。(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	public class Room
	{
		public Room()
		{}
		#region Model
		private int _roomid;
		private string _roomname;
		/// <summary>
		/// 
		/// </summary>
		public int RoomID
		{
			set{ _roomid=value;}
			get{return _roomid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoomName
		{
			set{ _roomname=value;}
			get{return _roomname;}
		}
		#endregion Model

	}
}

