﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 考勤记录信息(门禁里的记录信息)
	/// </summary>
	[Serializable]
	public partial class Kqjl_Info
	{
		public Kqjl_Info()
		{}
		#region Model
		private long _kqlsh;
		private int? _jsdsblsh;
		private string _jsdm;
		private string _sfzh;
		private string _xm;
		private string _qybmm;
		private string _gz;
		private string _fbm;
		private string _kh;
		private string _kbs;
		private DateTime? _kqsj;
		private bool _jczt;
		private bool _sfdj;
		private string _kqlx;
		private string _yyxp;
		private string _zpxp;
		private bool _sfff= false;
		/// <summary>
		/// 
		/// </summary>
		public long kqlsh
		{
			set{ _kqlsh=value;}
			get{return _kqlsh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? jsdsblsh
		{
			set{ _jsdsblsh=value;}
			get{return _jsdsblsh;}
		}
		/// <summary>
		/// 监视点设备流水号
		/// </summary>
		public string jsdm
		{
			set{ _jsdm=value;}
			get{return _jsdm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qybmm
		{
			set{ _qybmm=value;}
			get{return _qybmm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz
		{
			set{ _gz=value;}
			get{return _gz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string fbm
		{
			set{ _fbm=value;}
			get{return _fbm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string kh
		{
			set{ _kh=value;}
			get{return _kh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string kbs
		{
			set{ _kbs=value;}
			get{return _kbs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? kqsj
		{
			set{ _kqsj=value;}
			get{return _kqsj;}
		}
		/// <summary>
		/// 1：进 0：出
		/// </summary>
		public bool jczt
		{
			set{ _jczt=value;}
			get{return _jczt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sfdj
		{
			set{ _sfdj=value;}
			get{return _sfdj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string kqlx
		{
			set{ _kqlx=value;}
			get{return _kqlx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string yyxp
		{
			set{ _yyxp=value;}
			get{return _yyxp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zpxp
		{
			set{ _zpxp=value;}
			get{return _zpxp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sfff
		{
			set{ _sfff=value;}
			get{return _sfff;}
		}
		#endregion Model

	}
}

