﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 分步计算人员工时存储
	/// </summary>
	[Serializable]
	public partial class Compute_Rygs
	{
		public Compute_Rygs()
		{}
		#region Model
		private int _id;
		private string _sfzh;
		private string _xm;
		private string _qybmm;
		private decimal? _ljgs;
		private DateTime? _kqsj;
		private bool _jslx;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qybmm
		{
			set{ _qybmm=value;}
			get{return _qybmm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ljgs
		{
			set{ _ljgs=value;}
			get{return _ljgs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? kqsj
		{
			set{ _kqsj=value;}
			get{return _kqsj;}
		}
		/// <summary>
		/// 1：根据奇偶判断进出 0：根据进出情况
		/// </summary>
		public bool jslx
		{
			set{ _jslx=value;}
			get{return _jslx;}
		}
		#endregion Model

	}
}

