﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 人员所属卡信息
	/// </summary>
	[Serializable]
	public partial class RyCa_Info
	{
		public RyCa_Info()
		{}
		#region Model
		private long _lsh;
		private string _sfzh;
		private string _kh;
		private DateTime? _fksj;
		private DateTime? _cxsj;
		/// <summary>
		/// 
		/// </summary>
		public long lsh
		{
			set{ _lsh=value;}
			get{return _lsh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 卡CSN号
		/// </summary>
		public string kh
		{
			set{ _kh=value;}
			get{return _kh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? fksj
		{
			set{ _fksj=value;}
			get{return _fksj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? cxsj
		{
			set{ _cxsj=value;}
			get{return _cxsj;}
		}
		#endregion Model

	}
}

