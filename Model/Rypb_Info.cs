﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 人员排班信息
	/// </summary>
	[Serializable]
	public partial class Rypb_Info
	{
		public Rypb_Info()
		{}
		#region Model
		private string _sfzh;
		private string _xm;
		private string _qybmm;
		private DateTime _pbsj;
		private int? _bcid;
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string qybmm
		{
			set{ _qybmm=value;}
			get{return _qybmm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime pbsj
		{
			set{ _pbsj=value;}
			get{return _pbsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? bcid
		{
			set{ _bcid=value;}
			get{return _bcid;}
		}
		#endregion Model

	}
}

