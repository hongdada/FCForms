using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 实体类Ry_Stay_Info 。(属性说明自动提取数据库字段的描述信息)
	/// </summary>
    [Serializable]
	public class Ry_Stay_Info
	{
		public Ry_Stay_Info()
		{}
		#region Model
		private int _id;
		private string _xm;
		private string _sfzh;
		private int _state;
		private string _wza;
		private string _wzb;
		private DateTime _createtime;
		private DateTime _lasttime;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 姓名
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 身份证号
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 1 在住 2外住 
		/// </summary>
		public int state
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 幢
		/// </summary>
		public string wza
		{
			set{ _wza=value;}
			get{return _wza;}
		}
		/// <summary>
		/// 室
		/// </summary>
		public string wzb
		{
			set{ _wzb=value;}
			get{return _wzb;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 最后修改时间
		/// </summary>
		public DateTime LastTime
		{
			set{ _lasttime=value;}
			get{return _lasttime;}
		}
		#endregion Model

	}
}

