﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// Banz_Info:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Banz_Info
	{
		public Banz_Info()
		{}
		#region Model
		private string _bzid;
		private string _bzfid;
		private string _bzmc;
		private string _bz;
		/// <summary>
		/// 
		/// </summary>
		public string bzid
		{
			set{ _bzid=value;}
			get{return _bzid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bzfid
		{
			set{ _bzfid=value;}
			get{return _bzfid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bzmc
		{
			set{ _bzmc=value;}
			get{return _bzmc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bz
		{
			set{ _bz=value;}
			get{return _bz;}
		}
		#endregion Model

	}
}

