﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 记录所有设备共同信息
	/// </summary>
	[Serializable]
	public partial class Sb_Info
	{
		public Sb_Info()
		{}
		#region Model
		private int _sblsh;
		private string _sbbs;
		private int? _sbh;
		private int? _sbyt;
		private int? _sblx;
		private int? _kzlx;
		private int? _ljlx;
		private string _comip;
		private string _bddk;
		private int? _sjtbzt=3;
		/// <summary>
		/// 
		/// </summary>
		public int sblsh
		{
			set{ _sblsh=value;}
			get{return _sblsh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sbbs
		{
			set{ _sbbs=value;}
			get{return _sbbs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? sbh
		{
			set{ _sbh=value;}
			get{return _sbh;}
		}
		/// <summary>
		/// 0：门禁 1：通用
		/// </summary>
		public int? sbyt
		{
			set{ _sbyt=value;}
			get{return _sbyt;}
		}
		/// <summary>
		/// 0：中控 1：广安达 2:LED 3:DVR 4:兴致读卡器 5:复旦微电子读卡器 6:C3
		/// </summary>
		public int? sblx
		{
			set{ _sblx=value;}
			get{return _sblx;}
		}
		/// <summary>
		/// 0:单门单向 1:单门双向 2:双门单向 3:双门双向 4:四门单向
		/// </summary>
		public int? kzlx
		{
			set{ _kzlx=value;}
			get{return _kzlx;}
		}
		/// <summary>
		/// 连接类型 0:RS232/RS485 1:TCP/IP 2:USB
		/// </summary>
		public int? ljlx
		{
			set{ _ljlx=value;}
			get{return _ljlx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string comip
		{
			set{ _comip=value;}
			get{return _comip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string bddk
		{
			set{ _bddk=value;}
			get{return _bddk;}
		}
		/// <summary>
		/// 0:未同步  1:同步失败 2:同步成功 3:新增或同步失败后变更或未同步后变更
		/// </summary>
		public int? sjtbzt
		{
			set{ _sjtbzt=value;}
			get{return _sjtbzt;}
		}
		#endregion Model

	}
}

