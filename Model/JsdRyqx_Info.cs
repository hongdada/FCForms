﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视点人员权限信息
	/// </summary>
	[Serializable]
	public partial class JsdRyqx_Info
	{
		public JsdRyqx_Info()
		{}
		#region Model
		private string _jsdbh;
		private string _sfzh;
		private string _kh;
		private int? _ryid;
		private bool _sfhbmd;
		private bool _sfkqfq;
		private DateTime? _yxjcsj;
		/// <summary>
		/// 监视点编号
		/// </summary>
		public string jsdbh
		{
			set{ _jsdbh=value;}
			get{return _jsdbh;}
		}
		/// <summary>
		/// 如果是单机版为卡号如果是联机版为身份证号
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string kh
		{
			set{ _kh=value;}
			get{return _kh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ryid
		{
			set{ _ryid=value;}
			get{return _ryid;}
		}
		/// <summary>
		/// 是否黑白名单（西可需要，西可权限分为能进权限，在能进里再分黑白名单）
		/// </summary>
		public bool sfhbmd
		{
			set{ _sfhbmd=value;}
			get{return _sfhbmd;}
		}
		/// <summary>
		/// 是否开启反潜
		/// </summary>
		public bool sfkqfq
		{
			set{ _sfkqfq=value;}
			get{return _sfkqfq;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? yxjcsj
		{
			set{ _yxjcsj=value;}
			get{return _yxjcsj;}
		}
		#endregion Model

	}
}

