﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 监视点设备信息
	/// </summary>
	[Serializable]
	public partial class Jsdsb_Info
	{
		public Jsdsb_Info()
		{}
		#region Model
		private int _jsdsblsh;
		private string _jsdbh;
		private int? _sblsh;
		private bool _sfkqkq;
		private bool _sfkqjs;
		/// <summary>
		/// 监视点设备流水号
		/// </summary>
		public int jsdsblsh
		{
			set{ _jsdsblsh=value;}
			get{return _jsdsblsh;}
		}
		/// <summary>
		/// 监视点编号
		/// </summary>
		public string jsdbh
		{
			set{ _jsdbh=value;}
			get{return _jsdbh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? sblsh
		{
			set{ _sblsh=value;}
			get{return _sblsh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sfkqkq
		{
			set{ _sfkqkq=value;}
			get{return _sfkqkq;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool sfkqjs
		{
			set{ _sfkqjs=value;}
			get{return _sfkqjs;}
		}
		#endregion Model

	}
}

