﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 记录所有人员基本信息
	/// </summary>
	[Serializable]
	public partial class Ry_Info
	{
		public Ry_Info()
		{}
		#region Model
		private string _sfzh;
		private string _xm;
		private bool _xb;
		private string _jg;
		private string _gh;
		private DateTime? _csrq;
		private string _mz;
		private bool _ywpx;
		private string _xl;
		private string _zz;
		private string _lxdh;
		private string _jjllr;
		private string _lldh;
		private string _xp;
		private string _gz1;
		private string _gz2;
		private string _gz3;
		private string _gz4;
		private string _gz5;
		private bool _sfxyg;
		private bool _ygzt;
		private string _sgz;
		private string _jndj1;
		private string _jndj2;
		private string _jndj3;
		private DateTime? _createtime= DateTime.Now;
		private string _htbh;
		private string _xx;
		private string _aqjycj;
		private string _htyjpath;
		private DateTime? _intime;
		private DateTime? _outtime;
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool xb
		{
			set{ _xb=value;}
			get{return _xb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jg
		{
			set{ _jg=value;}
			get{return _jg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gh
		{
			set{ _gh=value;}
			get{return _gh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? csrq
		{
			set{ _csrq=value;}
			get{return _csrq;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mz
		{
			set{ _mz=value;}
			get{return _mz;}
		}
		/// <summary>
		/// 0:无 1:有
		/// </summary>
		public bool ywpx
		{
			set{ _ywpx=value;}
			get{return _ywpx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xl
		{
			set{ _xl=value;}
			get{return _xl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zz
		{
			set{ _zz=value;}
			get{return _zz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lxdh
		{
			set{ _lxdh=value;}
			get{return _lxdh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jjllr
		{
			set{ _jjllr=value;}
			get{return _jjllr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lldh
		{
			set{ _lldh=value;}
			get{return _lldh;}
		}
		/// <summary>
		/// 保存相片相对路径
		/// </summary>
		public string xp
		{
			set{ _xp=value;}
			get{return _xp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz1
		{
			set{ _gz1=value;}
			get{return _gz1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz2
		{
			set{ _gz2=value;}
			get{return _gz2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz3
		{
			set{ _gz3=value;}
			get{return _gz3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz4
		{
			set{ _gz4=value;}
			get{return _gz4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string gz5
		{
			set{ _gz5=value;}
			get{return _gz5;}
		}
		/// <summary>
		/// 1:新用工 0：非新用工
		/// </summary>
		public bool sfxyg
		{
			set{ _sfxyg=value;}
			get{return _sfxyg;}
		}
		/// <summary>
		/// 0:退工 1：用工
		/// </summary>
		public bool ygzt
		{
			set{ _ygzt=value;}
			get{return _ygzt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sgz
		{
			set{ _sgz=value;}
			get{return _sgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jndj1
		{
			set{ _jndj1=value;}
			get{return _jndj1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jndj2
		{
			set{ _jndj2=value;}
			get{return _jndj2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jndj3
		{
			set{ _jndj3=value;}
			get{return _jndj3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? createtime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string htbh
		{
			set{ _htbh=value;}
			get{return _htbh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xx
		{
			set{ _xx=value;}
			get{return _xx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string aqjycj
		{
			set{ _aqjycj=value;}
			get{return _aqjycj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string htyjpath
		{
			set{ _htyjpath=value;}
			get{return _htyjpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? intime
		{
			set{ _intime=value;}
			get{return _intime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? outtime
		{
			set{ _outtime=value;}
			get{return _outtime;}
		}
		#endregion Model

	}
}

