﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 人员所属卡信息
	/// </summary>
	[Serializable]
	public partial class RyFace_Info
	{
		public RyFace_Info()
		{}
		#region Model
		private long _mbsjlsh;
		private string _sfzh;
		private string _xm;
		private int? _sbryh;
		private string _mbsj;
		private DateTime? _cjsj;
		private DateTime? _ppsj;
		/// <summary>
		/// 
		/// </summary>
		public long mbsjlsh
		{
			set{ _mbsjlsh=value;}
			get{return _mbsjlsh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xm
		{
			set{ _xm=value;}
			get{return _xm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? sbryh
		{
			set{ _sbryh=value;}
			get{return _sbryh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mbsj
		{
			set{ _mbsj=value;}
			get{return _mbsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? cjsj
		{
			set{ _cjsj=value;}
			get{return _cjsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ppsj
		{
			set{ _ppsj=value;}
			get{return _ppsj;}
		}
		#endregion Model

	}
}

