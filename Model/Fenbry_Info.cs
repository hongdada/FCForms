﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 数据库的版本维护
	/// </summary>
	[Serializable]
	public partial class Fenbry_Info
	{
		public Fenbry_Info()
		{}
		#region Model
		private string _fbid;
		private string _sfzh;
		/// <summary>
		/// 
		/// </summary>
		public string fbid
		{
			set{ _fbid=value;}
			get{return _fbid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		#endregion Model

	}
}

