﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// 数据库的版本维护
	/// </summary>
	[Serializable]
	public partial class DVR_Set
	{
		public DVR_Set()
		{}
		#region Model
		private string _mykey;
		private string _myvalue;
		private bool _type;
		/// <summary>
		/// 
		/// </summary>
		public string MyKey
		{
			set{ _mykey=value;}
			get{return _mykey;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MyValue
		{
			set{ _myvalue=value;}
			get{return _myvalue;}
		}
		/// <summary>
		/// 0:用户名 1：频道
		/// </summary>
		public bool Type
		{
			set{ _type=value;}
			get{return _type;}
		}
		#endregion Model

	}
}

