﻿using System;
namespace Maticsoft.Model
{
	/// <summary>
	/// Bzry_Info:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Bzry_Info
	{
		public Bzry_Info()
		{}
		#region Model
		private string _bzid;
		private string _sfzh;
		/// <summary>
		/// 
		/// </summary>
		public string bzid
		{
			set{ _bzid=value;}
			get{return _bzid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sfzh
		{
			set{ _sfzh=value;}
			get{return _sfzh;}
		}
		#endregion Model

	}
}

