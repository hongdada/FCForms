﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Sys_Info
	/// </summary>
	public partial class Sys_Info
	{
		public Sys_Info()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Sys_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Sys_Info(");
			strSql.Append("gcmc,zbgs,kqgrdh,bflj,bfsj)");
			strSql.Append(" values (");
			strSql.Append("@gcmc,@zbgs,@kqgrdh,@bflj,@bfsj)");
			SqlParameter[] parameters = {
					new SqlParameter("@gcmc", SqlDbType.VarChar,200),
					new SqlParameter("@zbgs", SqlDbType.VarChar,100),
					new SqlParameter("@kqgrdh", SqlDbType.Int,4),
					new SqlParameter("@bflj", SqlDbType.VarChar,200),
					new SqlParameter("@bfsj", SqlDbType.VarChar,50)};
			parameters[0].Value = model.gcmc;
			parameters[1].Value = model.zbgs;
			parameters[2].Value = model.kqgrdh;
			parameters[3].Value = model.bflj;
			parameters[4].Value = model.bfsj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Sys_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Sys_Info set ");
			strSql.Append("gcmc=@gcmc,");
			strSql.Append("zbgs=@zbgs,");
			strSql.Append("kqgrdh=@kqgrdh,");
			strSql.Append("bflj=@bflj,");
			strSql.Append("bfsj=@bfsj");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@gcmc", SqlDbType.VarChar,200),
					new SqlParameter("@zbgs", SqlDbType.VarChar,100),
					new SqlParameter("@kqgrdh", SqlDbType.Int,4),
					new SqlParameter("@bflj", SqlDbType.VarChar,200),
					new SqlParameter("@bfsj", SqlDbType.VarChar,50)};
			parameters[0].Value = model.gcmc;
			parameters[1].Value = model.zbgs;
			parameters[2].Value = model.kqgrdh;
			parameters[3].Value = model.bflj;
			parameters[4].Value = model.bfsj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sys_Info ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Sys_Info GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 gcmc,zbgs,kqgrdh,bflj,bfsj from Sys_Info ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			Maticsoft.Model.Sys_Info model=new Maticsoft.Model.Sys_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Sys_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Sys_Info model=new Maticsoft.Model.Sys_Info();
			if (row != null)
			{
				if(row["gcmc"]!=null)
				{
					model.gcmc=row["gcmc"].ToString();
				}
				if(row["zbgs"]!=null)
				{
					model.zbgs=row["zbgs"].ToString();
				}
				if(row["kqgrdh"]!=null && row["kqgrdh"].ToString()!="")
				{
					model.kqgrdh=int.Parse(row["kqgrdh"].ToString());
				}
				if(row["bflj"]!=null)
				{
					model.bflj=row["bflj"].ToString();
				}
				if(row["bfsj"]!=null)
				{
					model.bfsj=row["bfsj"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select gcmc,zbgs,kqgrdh,bflj,bfsj ");
			strSql.Append(" FROM Sys_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" gcmc,zbgs,kqgrdh,bflj,bfsj ");
			strSql.Append(" FROM Sys_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Sys_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sblsh desc");
			}
			strSql.Append(")AS Row, T.*  from Sys_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Sys_Info";
			parameters[1].Value = "sblsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

