﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Rypb_Info
	/// </summary>
	public partial class Rypb_Info
	{
		public Rypb_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string sfzh,DateTime pbsj)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Rypb_Info");
			strSql.Append(" where sfzh=@sfzh and pbsj=@pbsj ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@pbsj", SqlDbType.DateTime)			};
			parameters[0].Value = sfzh;
			parameters[1].Value = pbsj;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Rypb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Rypb_Info(");
			strSql.Append("sfzh,xm,qybmm,pbsj,bcid)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@xm,@qybmm,@pbsj,@bcid)");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@pbsj", SqlDbType.DateTime),
					new SqlParameter("@bcid", SqlDbType.Int,4)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.xm;
			parameters[2].Value = model.qybmm;
			parameters[3].Value = model.pbsj;
			parameters[4].Value = model.bcid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Rypb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Rypb_Info set ");
			strSql.Append("xm=@xm,");
			strSql.Append("qybmm=@qybmm,");
			strSql.Append("bcid=@bcid");
			strSql.Append(" where sfzh=@sfzh and pbsj=@pbsj ");
			SqlParameter[] parameters = {
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@bcid", SqlDbType.Int,4),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@pbsj", SqlDbType.DateTime)};
			parameters[0].Value = model.xm;
			parameters[1].Value = model.qybmm;
			parameters[2].Value = model.bcid;
			parameters[3].Value = model.sfzh;
			parameters[4].Value = model.pbsj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string sfzh,DateTime pbsj)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Rypb_Info ");
			strSql.Append(" where sfzh=@sfzh and pbsj=@pbsj ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@pbsj", SqlDbType.DateTime)			};
			parameters[0].Value = sfzh;
			parameters[1].Value = pbsj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Rypb_Info GetModel(string sfzh,DateTime pbsj)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sfzh,xm,qybmm,pbsj,bcid from Rypb_Info ");
			strSql.Append(" where sfzh=@sfzh and pbsj=@pbsj ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@pbsj", SqlDbType.DateTime)			};
			parameters[0].Value = sfzh;
			parameters[1].Value = pbsj;

			Maticsoft.Model.Rypb_Info model=new Maticsoft.Model.Rypb_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Rypb_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Rypb_Info model=new Maticsoft.Model.Rypb_Info();
			if (row != null)
			{
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["xm"]!=null)
				{
					model.xm=row["xm"].ToString();
				}
				if(row["qybmm"]!=null)
				{
					model.qybmm=row["qybmm"].ToString();
				}
				if(row["pbsj"]!=null && row["pbsj"].ToString()!="")
				{
					model.pbsj=DateTime.Parse(row["pbsj"].ToString());
				}
				if(row["bcid"]!=null && row["bcid"].ToString()!="")
				{
					model.bcid=int.Parse(row["bcid"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sfzh,xm,qybmm,pbsj,bcid ");
			strSql.Append(" FROM Rypb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sfzh,xm,qybmm,pbsj,bcid ");
			strSql.Append(" FROM Rypb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Rypb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pbsj desc");
			}
			strSql.Append(")AS Row, T.*  from Rypb_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Rypb_Info";
			parameters[1].Value = "pbsj";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

