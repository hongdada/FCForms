﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:DBVer
	/// </summary>
	public partial class DBVer
	{
		public DBVer()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string VerId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from DBVer");
			strSql.Append(" where VerId=@VerId ");
			SqlParameter[] parameters = {
					new SqlParameter("@VerId", SqlDbType.VarChar,5)			};
			parameters[0].Value = VerId;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.DBVer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into DBVer(");
			strSql.Append("VerId,SoftVerId)");
			strSql.Append(" values (");
			strSql.Append("@VerId,@SoftVerId)");
			SqlParameter[] parameters = {
					new SqlParameter("@VerId", SqlDbType.VarChar,5),
					new SqlParameter("@SoftVerId", SqlDbType.VarChar,10)};
			parameters[0].Value = model.VerId;
			parameters[1].Value = model.SoftVerId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.DBVer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update DBVer set ");
			strSql.Append("SoftVerId=@SoftVerId");
			strSql.Append(" where VerId=@VerId ");
			SqlParameter[] parameters = {
					new SqlParameter("@SoftVerId", SqlDbType.VarChar,10),
					new SqlParameter("@VerId", SqlDbType.VarChar,5)};
			parameters[0].Value = model.SoftVerId;
			parameters[1].Value = model.VerId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string VerId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from DBVer ");
			strSql.Append(" where VerId=@VerId ");
			SqlParameter[] parameters = {
					new SqlParameter("@VerId", SqlDbType.VarChar,5)			};
			parameters[0].Value = VerId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string VerIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from DBVer ");
			strSql.Append(" where VerId in ("+VerIdlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.DBVer GetModel(string VerId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 VerId,SoftVerId from DBVer ");
			strSql.Append(" where VerId=@VerId ");
			SqlParameter[] parameters = {
					new SqlParameter("@VerId", SqlDbType.VarChar,5)			};
			parameters[0].Value = VerId;

			Maticsoft.Model.DBVer model=new Maticsoft.Model.DBVer();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.DBVer DataRowToModel(DataRow row)
		{
			Maticsoft.Model.DBVer model=new Maticsoft.Model.DBVer();
			if (row != null)
			{
				if(row["VerId"]!=null)
				{
					model.VerId=row["VerId"].ToString();
				}
				if(row["SoftVerId"]!=null)
				{
					model.SoftVerId=row["SoftVerId"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select VerId,SoftVerId ");
			strSql.Append(" FROM DBVer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" VerId,SoftVerId ");
			strSql.Append(" FROM DBVer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM DBVer ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.VerId desc");
			}
			strSql.Append(")AS Row, T.*  from DBVer T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "DBVer";
			parameters[1].Value = "VerId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

