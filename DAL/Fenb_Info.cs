﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Fenb_Info
	/// </summary>
	public partial class Fenb_Info
	{
		public Fenb_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string fbid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Fenb_Info");
			strSql.Append(" where fbid=@fbid ");
			SqlParameter[] parameters = {
					new SqlParameter("@fbid", SqlDbType.VarChar,20)			};
			parameters[0].Value = fbid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Fenb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Fenb_Info(");
			strSql.Append("fbid,fbfid,fbmc,bz)");
			strSql.Append(" values (");
			strSql.Append("@fbid,@fbfid,@fbmc,@bz)");
			SqlParameter[] parameters = {
					new SqlParameter("@fbid", SqlDbType.VarChar,20),
					new SqlParameter("@fbfid", SqlDbType.VarChar,20),
					new SqlParameter("@fbmc", SqlDbType.VarChar,50),
					new SqlParameter("@bz", SqlDbType.VarChar,300)};
			parameters[0].Value = model.fbid;
			parameters[1].Value = model.fbfid;
			parameters[2].Value = model.fbmc;
			parameters[3].Value = model.bz;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Fenb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Fenb_Info set ");
			strSql.Append("fbfid=@fbfid,");
			strSql.Append("fbmc=@fbmc,");
			strSql.Append("bz=@bz");
			strSql.Append(" where fbid=@fbid ");
			SqlParameter[] parameters = {
					new SqlParameter("@fbfid", SqlDbType.VarChar,20),
					new SqlParameter("@fbmc", SqlDbType.VarChar,50),
					new SqlParameter("@bz", SqlDbType.VarChar,300),
					new SqlParameter("@fbid", SqlDbType.VarChar,20)};
			parameters[0].Value = model.fbfid;
			parameters[1].Value = model.fbmc;
			parameters[2].Value = model.bz;
			parameters[3].Value = model.fbid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string fbid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Fenb_Info ");
			strSql.Append(" where fbid=@fbid ");
			SqlParameter[] parameters = {
					new SqlParameter("@fbid", SqlDbType.VarChar,20)			};
			parameters[0].Value = fbid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string fbidlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Fenb_Info ");
			strSql.Append(" where fbid in ("+fbidlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Fenb_Info GetModel(string fbid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 fbid,fbfid,fbmc,bz from Fenb_Info ");
			strSql.Append(" where fbid=@fbid ");
			SqlParameter[] parameters = {
					new SqlParameter("@fbid", SqlDbType.VarChar,20)			};
			parameters[0].Value = fbid;

			Maticsoft.Model.Fenb_Info model=new Maticsoft.Model.Fenb_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Fenb_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Fenb_Info model=new Maticsoft.Model.Fenb_Info();
			if (row != null)
			{
				if(row["fbid"]!=null)
				{
					model.fbid=row["fbid"].ToString();
				}
				if(row["fbfid"]!=null)
				{
					model.fbfid=row["fbfid"].ToString();
				}
				if(row["fbmc"]!=null)
				{
					model.fbmc=row["fbmc"].ToString();
				}
				if(row["bz"]!=null)
				{
					model.bz=row["bz"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select fbid,fbfid,fbmc,bz ");
			strSql.Append(" FROM Fenb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" fbid,fbfid,fbmc,bz ");
			strSql.Append(" FROM Fenb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Fenb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.fbid desc");
			}
			strSql.Append(")AS Row, T.*  from Fenb_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Fenb_Info";
			parameters[1].Value = "fbid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

