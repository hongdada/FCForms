﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:RyFace_Info
	/// </summary>
	public partial class RyFace_Info
	{
		public RyFace_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long mbsjlsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from RyFace_Info");
			strSql.Append(" where mbsjlsh=@mbsjlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@mbsjlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = mbsjlsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(Maticsoft.Model.RyFace_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into RyFace_Info(");
			strSql.Append("sfzh,xm,sbryh,mbsj,cjsj,ppsj)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@xm,@sbryh,@mbsj,@cjsj,@ppsj)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@sbryh", SqlDbType.Int,4),
					new SqlParameter("@mbsj", SqlDbType.VarChar,-1),
					new SqlParameter("@cjsj", SqlDbType.DateTime),
					new SqlParameter("@ppsj", SqlDbType.DateTime)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.xm;
			parameters[2].Value = model.sbryh;
			parameters[3].Value = model.mbsj;
			parameters[4].Value = model.cjsj;
			parameters[5].Value = model.ppsj;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt64(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.RyFace_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update RyFace_Info set ");
			strSql.Append("sfzh=@sfzh,");
			strSql.Append("xm=@xm,");
			strSql.Append("sbryh=@sbryh,");
			strSql.Append("mbsj=@mbsj,");
			strSql.Append("cjsj=@cjsj,");
			strSql.Append("ppsj=@ppsj");
			strSql.Append(" where mbsjlsh=@mbsjlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@sbryh", SqlDbType.Int,4),
					new SqlParameter("@mbsj", SqlDbType.VarChar,-1),
					new SqlParameter("@cjsj", SqlDbType.DateTime),
					new SqlParameter("@ppsj", SqlDbType.DateTime),
					new SqlParameter("@mbsjlsh", SqlDbType.BigInt,8)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.xm;
			parameters[2].Value = model.sbryh;
			parameters[3].Value = model.mbsj;
			parameters[4].Value = model.cjsj;
			parameters[5].Value = model.ppsj;
			parameters[6].Value = model.mbsjlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long mbsjlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyFace_Info ");
			strSql.Append(" where mbsjlsh=@mbsjlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@mbsjlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = mbsjlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string mbsjlshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyFace_Info ");
			strSql.Append(" where mbsjlsh in ("+mbsjlshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyFace_Info GetModel(long mbsjlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 mbsjlsh,sfzh,xm,sbryh,mbsj,cjsj,ppsj from RyFace_Info ");
			strSql.Append(" where mbsjlsh=@mbsjlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@mbsjlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = mbsjlsh;

			Maticsoft.Model.RyFace_Info model=new Maticsoft.Model.RyFace_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyFace_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.RyFace_Info model=new Maticsoft.Model.RyFace_Info();
			if (row != null)
			{
				if(row["mbsjlsh"]!=null && row["mbsjlsh"].ToString()!="")
				{
					model.mbsjlsh=long.Parse(row["mbsjlsh"].ToString());
				}
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["xm"]!=null)
				{
					model.xm=row["xm"].ToString();
				}
				if(row["sbryh"]!=null && row["sbryh"].ToString()!="")
				{
					model.sbryh=int.Parse(row["sbryh"].ToString());
				}
				if(row["mbsj"]!=null)
				{
					model.mbsj=row["mbsj"].ToString();
				}
				if(row["cjsj"]!=null && row["cjsj"].ToString()!="")
				{
					model.cjsj=DateTime.Parse(row["cjsj"].ToString());
				}
				if(row["ppsj"]!=null && row["ppsj"].ToString()!="")
				{
					model.ppsj=DateTime.Parse(row["ppsj"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select mbsjlsh,sfzh,xm,sbryh,mbsj,cjsj,ppsj ");
			strSql.Append(" FROM RyFace_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" mbsjlsh,sfzh,xm,sbryh,mbsj,cjsj,ppsj ");
			strSql.Append(" FROM RyFace_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM RyFace_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.mbsjlsh desc");
			}
			strSql.Append(")AS Row, T.*  from RyFace_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "RyFace_Info";
			parameters[1].Value = "mbsjlsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

