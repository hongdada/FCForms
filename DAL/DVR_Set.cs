﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:DVR_Set
	/// </summary>
	public partial class DVR_Set
	{
		public DVR_Set()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string MyKey)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from DVR_Set");
			strSql.Append(" where MyKey=@MyKey ");
			SqlParameter[] parameters = {
					new SqlParameter("@MyKey", SqlDbType.VarChar,50)			};
			parameters[0].Value = MyKey;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.DVR_Set model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into DVR_Set(");
			strSql.Append("MyKey,MyValue,Type)");
			strSql.Append(" values (");
			strSql.Append("@MyKey,@MyValue,@Type)");
			SqlParameter[] parameters = {
					new SqlParameter("@MyKey", SqlDbType.VarChar,50),
					new SqlParameter("@MyValue", SqlDbType.VarChar,50),
					new SqlParameter("@Type", SqlDbType.Bit,1)};
			parameters[0].Value = model.MyKey;
			parameters[1].Value = model.MyValue;
			parameters[2].Value = model.Type;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.DVR_Set model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update DVR_Set set ");
			strSql.Append("MyValue=@MyValue,");
			strSql.Append("Type=@Type");
			strSql.Append(" where MyKey=@MyKey ");
			SqlParameter[] parameters = {
					new SqlParameter("@MyValue", SqlDbType.VarChar,50),
					new SqlParameter("@Type", SqlDbType.Bit,1),
					new SqlParameter("@MyKey", SqlDbType.VarChar,50)};
			parameters[0].Value = model.MyValue;
			parameters[1].Value = model.Type;
			parameters[2].Value = model.MyKey;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string MyKey)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from DVR_Set ");
			strSql.Append(" where MyKey=@MyKey ");
			SqlParameter[] parameters = {
					new SqlParameter("@MyKey", SqlDbType.VarChar,50)			};
			parameters[0].Value = MyKey;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string MyKeylist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from DVR_Set ");
			strSql.Append(" where MyKey in ("+MyKeylist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.DVR_Set GetModel(string MyKey)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 MyKey,MyValue,Type from DVR_Set ");
			strSql.Append(" where MyKey=@MyKey ");
			SqlParameter[] parameters = {
					new SqlParameter("@MyKey", SqlDbType.VarChar,50)			};
			parameters[0].Value = MyKey;

			Maticsoft.Model.DVR_Set model=new Maticsoft.Model.DVR_Set();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.DVR_Set DataRowToModel(DataRow row)
		{
			Maticsoft.Model.DVR_Set model=new Maticsoft.Model.DVR_Set();
			if (row != null)
			{
				if(row["MyKey"]!=null)
				{
					model.MyKey=row["MyKey"].ToString();
				}
				if(row["MyValue"]!=null)
				{
					model.MyValue=row["MyValue"].ToString();
				}
				if(row["Type"]!=null && row["Type"].ToString()!="")
				{
					if((row["Type"].ToString()=="1")||(row["Type"].ToString().ToLower()=="true"))
					{
						model.Type=true;
					}
					else
					{
						model.Type=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select MyKey,MyValue,Type ");
			strSql.Append(" FROM DVR_Set ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" MyKey,MyValue,Type ");
			strSql.Append(" FROM DVR_Set ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM DVR_Set ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.MyKey desc");
			}
			strSql.Append(")AS Row, T.*  from DVR_Set T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "DVR_Set";
			parameters[1].Value = "MyKey";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

