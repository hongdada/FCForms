using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//请先添加引用
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类Building。
	/// </summary>
	public class Building
	{
		public Building()
		{}
		#region  成员方法

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("BuildingID", "Building"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int BuildingID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Building");
			strSql.Append(" where BuildingID=@BuildingID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BuildingID", SqlDbType.Int,4)};
			parameters[0].Value = BuildingID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Building model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Building(");
			strSql.Append("BuildingName)");
			strSql.Append(" values (");
			strSql.Append("@BuildingName)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,50)};
			parameters[0].Value = model.BuildingName;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 1;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public void Update(Maticsoft.Model.Building model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Building set ");
			strSql.Append("BuildingName=@BuildingName");
			strSql.Append(" where BuildingID=@BuildingID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BuildingID", SqlDbType.Int,4),
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,50)};
			parameters[0].Value = model.BuildingID;
			parameters[1].Value = model.BuildingName;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public void Delete(int BuildingID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete Building ");
			strSql.Append(" where BuildingID=@BuildingID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BuildingID", SqlDbType.Int,4)};
			parameters[0].Value = BuildingID;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}

        /// <summary>
        /// 删除数据
        /// </summary>
        public bool DeleteByWhere(string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete Building ");
            if (!string.IsNullOrEmpty(where))
            {
                strSql.Append(" where " + where);
            }
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Building GetModel(int BuildingID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select BuildingID,BuildingName from Building ");
			strSql.Append(" where BuildingID=@BuildingID ");
			SqlParameter[] parameters = {
					new SqlParameter("@BuildingID", SqlDbType.Int,4)};
			parameters[0].Value = BuildingID;

			Maticsoft.Model.Building model=new Maticsoft.Model.Building();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["BuildingID"].ToString()!="")
				{
					model.BuildingID=int.Parse(ds.Tables[0].Rows[0]["BuildingID"].ToString());
				}
				model.BuildingName=ds.Tables[0].Rows[0]["BuildingName"].ToString();
				return model;
			}
			else
			{
			return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select BuildingID,BuildingName ");
			strSql.Append(" FROM Building ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Building";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  成员方法
	}
}

