﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Ca_Info
	/// </summary>
	public partial class Ca_Info
	{
		public Ca_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string kh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ca_Info");
			strSql.Append(" where kh=@kh ");
			SqlParameter[] parameters = {
					new SqlParameter("@kh", SqlDbType.VarChar,10)			};
			parameters[0].Value = kh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Ca_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ca_Info(");
			strSql.Append("kh,kbs,klx,sfyx)");
			strSql.Append(" values (");
			strSql.Append("@kh,@kbs,@klx,@sfyx)");
			SqlParameter[] parameters = {
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@klx", SqlDbType.Int,4),
					new SqlParameter("@sfyx", SqlDbType.Bit,1)};
			parameters[0].Value = model.kh;
			parameters[1].Value = model.kbs;
			parameters[2].Value = model.klx;
			parameters[3].Value = model.sfyx;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Ca_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ca_Info set ");
			strSql.Append("kbs=@kbs,");
			strSql.Append("klx=@klx,");
			strSql.Append("sfyx=@sfyx");
			strSql.Append(" where kh=@kh ");
			SqlParameter[] parameters = {
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@klx", SqlDbType.Int,4),
					new SqlParameter("@sfyx", SqlDbType.Bit,1),
					new SqlParameter("@kh", SqlDbType.VarChar,10)};
			parameters[0].Value = model.kbs;
			parameters[1].Value = model.klx;
			parameters[2].Value = model.sfyx;
			parameters[3].Value = model.kh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string kh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ca_Info ");
			strSql.Append(" where kh=@kh ");
			SqlParameter[] parameters = {
					new SqlParameter("@kh", SqlDbType.VarChar,10)			};
			parameters[0].Value = kh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string khlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ca_Info ");
			strSql.Append(" where kh in ("+khlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Ca_Info GetModel(string kh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 kh,kbs,klx,sfyx from Ca_Info ");
			strSql.Append(" where kh=@kh ");
			SqlParameter[] parameters = {
					new SqlParameter("@kh", SqlDbType.VarChar,10)			};
			parameters[0].Value = kh;

			Maticsoft.Model.Ca_Info model=new Maticsoft.Model.Ca_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Ca_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Ca_Info model=new Maticsoft.Model.Ca_Info();
			if (row != null)
			{
				if(row["kh"]!=null)
				{
					model.kh=row["kh"].ToString();
				}
				if(row["kbs"]!=null)
				{
					model.kbs=row["kbs"].ToString();
				}
				if(row["klx"]!=null && row["klx"].ToString()!="")
				{
					model.klx=int.Parse(row["klx"].ToString());
				}
				if(row["sfyx"]!=null && row["sfyx"].ToString()!="")
				{
					if((row["sfyx"].ToString()=="1")||(row["sfyx"].ToString().ToLower()=="true"))
					{
						model.sfyx=true;
					}
					else
					{
						model.sfyx=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select kh,kbs,klx,sfyx ");
			strSql.Append(" FROM Ca_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" kh,kbs,klx,sfyx ");
			strSql.Append(" FROM Ca_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ca_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.kh desc");
			}
			strSql.Append(")AS Row, T.*  from Ca_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ca_Info";
			parameters[1].Value = "kh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

