﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Jsd_Info
	/// </summary>
	public partial class Jsd_Info
	{
		public Jsd_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string jsdbh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Jsd_Info");
			strSql.Append(" where jsdbh=@jsdbh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Jsd_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Jsd_Info(");
			strSql.Append("jsdbh,jsdfbh,jsdm,bz,lx)");
			strSql.Append(" values (");
			strSql.Append("@jsdbh,@jsdfbh,@jsdm,@bz,@lx)");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@jsdfbh", SqlDbType.VarChar,18),
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@bz", SqlDbType.VarChar,400),
					new SqlParameter("@lx", SqlDbType.SmallInt,2)};
			parameters[0].Value = model.jsdbh;
			parameters[1].Value = model.jsdfbh;
			parameters[2].Value = model.jsdm;
			parameters[3].Value = model.bz;
			parameters[4].Value = model.lx;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Jsd_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Jsd_Info set ");
			strSql.Append("jsdfbh=@jsdfbh,");
			strSql.Append("jsdm=@jsdm,");
			strSql.Append("bz=@bz,");
			strSql.Append("lx=@lx");
			strSql.Append(" where jsdbh=@jsdbh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdfbh", SqlDbType.VarChar,18),
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@bz", SqlDbType.VarChar,400),
					new SqlParameter("@lx", SqlDbType.SmallInt,2),
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18)};
			parameters[0].Value = model.jsdfbh;
			parameters[1].Value = model.jsdm;
			parameters[2].Value = model.bz;
			parameters[3].Value = model.lx;
			parameters[4].Value = model.jsdbh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string jsdbh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Jsd_Info ");
			strSql.Append(" where jsdbh=@jsdbh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string jsdbhlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Jsd_Info ");
			strSql.Append(" where jsdbh in ("+jsdbhlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Jsd_Info GetModel(string jsdbh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 jsdbh,jsdfbh,jsdm,bz,lx from Jsd_Info ");
			strSql.Append(" where jsdbh=@jsdbh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;

			Maticsoft.Model.Jsd_Info model=new Maticsoft.Model.Jsd_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Jsd_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Jsd_Info model=new Maticsoft.Model.Jsd_Info();
			if (row != null)
			{
				if(row["jsdbh"]!=null)
				{
					model.jsdbh=row["jsdbh"].ToString();
				}
				if(row["jsdfbh"]!=null)
				{
					model.jsdfbh=row["jsdfbh"].ToString();
				}
				if(row["jsdm"]!=null)
				{
					model.jsdm=row["jsdm"].ToString();
				}
				if(row["bz"]!=null)
				{
					model.bz=row["bz"].ToString();
				}
				if(row["lx"]!=null && row["lx"].ToString()!="")
				{
					model.lx=int.Parse(row["lx"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select jsdbh,jsdfbh,jsdm,bz,lx ");
			strSql.Append(" FROM Jsd_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" jsdbh,jsdfbh,jsdm,bz,lx ");
			strSql.Append(" FROM Jsd_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Jsd_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.jsdbh desc");
			}
			strSql.Append(")AS Row, T.*  from Jsd_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Jsd_Info";
			parameters[1].Value = "jsdbh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

