using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//请先添加引用
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类Room。
	/// </summary>
	public class Room
	{
		public Room()
		{}
		#region  成员方法

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("RoomID", "Room"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int RoomID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Room");
			strSql.Append(" where RoomID=@RoomID ");
			SqlParameter[] parameters = {
					new SqlParameter("@RoomID", SqlDbType.Int,4)};
			parameters[0].Value = RoomID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Room model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Room(");
			strSql.Append("RoomName)");
			strSql.Append(" values (");
			strSql.Append("@RoomName)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RoomName", SqlDbType.VarChar,50)};
			parameters[0].Value = model.RoomName;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 1;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public void Update(Maticsoft.Model.Room model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Room set ");
			strSql.Append("RoomName=@RoomName");
			strSql.Append(" where RoomID=@RoomID ");
			SqlParameter[] parameters = {
					new SqlParameter("@RoomID", SqlDbType.Int,4),
					new SqlParameter("@RoomName", SqlDbType.VarChar,50)};
			parameters[0].Value = model.RoomID;
			parameters[1].Value = model.RoomName;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public void Delete(int RoomID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete Room ");
			strSql.Append(" where RoomID=@RoomID ");
			SqlParameter[] parameters = {
					new SqlParameter("@RoomID", SqlDbType.Int,4)};
			parameters[0].Value = RoomID;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}

        /// <summary>
        /// 删除数据
        /// </summary>
        public bool DeleteByWhere(string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete Room ");
            if (!string.IsNullOrEmpty(where))
            {
                strSql.Append(" where " + where);
            }
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Room GetModel(int RoomID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select RoomID,RoomName from Room ");
			strSql.Append(" where RoomID=@RoomID ");
			SqlParameter[] parameters = {
					new SqlParameter("@RoomID", SqlDbType.Int,4)};
			parameters[0].Value = RoomID;

			Maticsoft.Model.Room model=new Maticsoft.Model.Room();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["RoomID"].ToString()!="")
				{
					model.RoomID=int.Parse(ds.Tables[0].Rows[0]["RoomID"].ToString());
				}
				model.RoomName=ds.Tables[0].Rows[0]["RoomName"].ToString();
				return model;
			}
			else
			{
			return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select RoomID,RoomName ");
			strSql.Append(" FROM Room ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Room";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  成员方法
	}
}

