﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Bc_Info
	/// </summary>
	public partial class Bc_Info
	{
		public Bc_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("bcid", "Bc_Info"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int bcid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Bc_Info");
			strSql.Append(" where bcid=@bcid");
			SqlParameter[] parameters = {
					new SqlParameter("@bcid", SqlDbType.Int,4)
			};
			parameters[0].Value = bcid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Bc_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Bc_Info(");
			strSql.Append("bcm,sjd1ks,sjd1js,sjd2ks,sjd2js,sjd3ks,sjd3js,sjd4ks,sjd4js)");
			strSql.Append(" values (");
			strSql.Append("@bcm,@sjd1ks,@sjd1js,@sjd2ks,@sjd2js,@sjd3ks,@sjd3js,@sjd4ks,@sjd4js)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@bcm", SqlDbType.VarChar,50),
					new SqlParameter("@sjd1ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd1js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd2ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd2js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd3ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd3js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd4ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd4js", SqlDbType.VarChar,6)};
			parameters[0].Value = model.bcm;
			parameters[1].Value = model.sjd1ks;
			parameters[2].Value = model.sjd1js;
			parameters[3].Value = model.sjd2ks;
			parameters[4].Value = model.sjd2js;
			parameters[5].Value = model.sjd3ks;
			parameters[6].Value = model.sjd3js;
			parameters[7].Value = model.sjd4ks;
			parameters[8].Value = model.sjd4js;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Bc_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Bc_Info set ");
			strSql.Append("bcm=@bcm,");
			strSql.Append("sjd1ks=@sjd1ks,");
			strSql.Append("sjd1js=@sjd1js,");
			strSql.Append("sjd2ks=@sjd2ks,");
			strSql.Append("sjd2js=@sjd2js,");
			strSql.Append("sjd3ks=@sjd3ks,");
			strSql.Append("sjd3js=@sjd3js,");
			strSql.Append("sjd4ks=@sjd4ks,");
			strSql.Append("sjd4js=@sjd4js");
			strSql.Append(" where bcid=@bcid");
			SqlParameter[] parameters = {
					new SqlParameter("@bcm", SqlDbType.VarChar,50),
					new SqlParameter("@sjd1ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd1js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd2ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd2js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd3ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd3js", SqlDbType.VarChar,6),
					new SqlParameter("@sjd4ks", SqlDbType.VarChar,6),
					new SqlParameter("@sjd4js", SqlDbType.VarChar,6),
					new SqlParameter("@bcid", SqlDbType.Int,4)};
			parameters[0].Value = model.bcm;
			parameters[1].Value = model.sjd1ks;
			parameters[2].Value = model.sjd1js;
			parameters[3].Value = model.sjd2ks;
			parameters[4].Value = model.sjd2js;
			parameters[5].Value = model.sjd3ks;
			parameters[6].Value = model.sjd3js;
			parameters[7].Value = model.sjd4ks;
			parameters[8].Value = model.sjd4js;
			parameters[9].Value = model.bcid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int bcid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Bc_Info ");
			strSql.Append(" where bcid=@bcid");
			SqlParameter[] parameters = {
					new SqlParameter("@bcid", SqlDbType.Int,4)
			};
			parameters[0].Value = bcid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string bcidlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Bc_Info ");
			strSql.Append(" where bcid in ("+bcidlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Bc_Info GetModel(int bcid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 bcid,bcm,sjd1ks,sjd1js,sjd2ks,sjd2js,sjd3ks,sjd3js,sjd4ks,sjd4js from Bc_Info ");
			strSql.Append(" where bcid=@bcid");
			SqlParameter[] parameters = {
					new SqlParameter("@bcid", SqlDbType.Int,4)
			};
			parameters[0].Value = bcid;

			Maticsoft.Model.Bc_Info model=new Maticsoft.Model.Bc_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Bc_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Bc_Info model=new Maticsoft.Model.Bc_Info();
			if (row != null)
			{
				if(row["bcid"]!=null && row["bcid"].ToString()!="")
				{
					model.bcid=int.Parse(row["bcid"].ToString());
				}
				if(row["bcm"]!=null)
				{
					model.bcm=row["bcm"].ToString();
				}
				if(row["sjd1ks"]!=null)
				{
					model.sjd1ks=row["sjd1ks"].ToString();
				}
				if(row["sjd1js"]!=null)
				{
					model.sjd1js=row["sjd1js"].ToString();
				}
				if(row["sjd2ks"]!=null)
				{
					model.sjd2ks=row["sjd2ks"].ToString();
				}
				if(row["sjd2js"]!=null)
				{
					model.sjd2js=row["sjd2js"].ToString();
				}
				if(row["sjd3ks"]!=null)
				{
					model.sjd3ks=row["sjd3ks"].ToString();
				}
				if(row["sjd3js"]!=null)
				{
					model.sjd3js=row["sjd3js"].ToString();
				}
				if(row["sjd4ks"]!=null)
				{
					model.sjd4ks=row["sjd4ks"].ToString();
				}
				if(row["sjd4js"]!=null)
				{
					model.sjd4js=row["sjd4js"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select bcid,bcm,sjd1ks,sjd1js,sjd2ks,sjd2js,sjd3ks,sjd3js,sjd4ks,sjd4js ");
			strSql.Append(" FROM Bc_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" bcid,bcm,sjd1ks,sjd1js,sjd2ks,sjd2js,sjd3ks,sjd3js,sjd4ks,sjd4js ");
			strSql.Append(" FROM Bc_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Bc_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.bcid desc");
			}
			strSql.Append(")AS Row, T.*  from Bc_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Bc_Info";
			parameters[1].Value = "bcid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

