﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Ry_Info
	/// </summary>
	public partial class Ry_Info
	{
		public Ry_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string sfzh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ry_Info");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Ry_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ry_Info(");
			strSql.Append("sfzh,xm,xb,jg,gh,csrq,mz,ywpx,xl,zz,lxdh,jjllr,lldh,xp,gz1,gz2,gz3,gz4,gz5,sfxyg,ygzt,sgz,jndj1,jndj2,jndj3,createtime,htbh,xx,aqjycj,htyjpath,intime,outtime)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@xm,@xb,@jg,@gh,@csrq,@mz,@ywpx,@xl,@zz,@lxdh,@jjllr,@lldh,@xp,@gz1,@gz2,@gz3,@gz4,@gz5,@sfxyg,@ygzt,@sgz,@jndj1,@jndj2,@jndj3,@createtime,@htbh,@xx,@aqjycj,@htyjpath,@intime,@outtime)");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@xb", SqlDbType.Bit,1),
					new SqlParameter("@jg", SqlDbType.VarChar,15),
					new SqlParameter("@gh", SqlDbType.VarChar,20),
					new SqlParameter("@csrq", SqlDbType.DateTime),
					new SqlParameter("@mz", SqlDbType.VarChar,10),
					new SqlParameter("@ywpx", SqlDbType.Bit,1),
					new SqlParameter("@xl", SqlDbType.VarChar,10),
					new SqlParameter("@zz", SqlDbType.VarChar,300),
					new SqlParameter("@lxdh", SqlDbType.VarChar,50),
					new SqlParameter("@jjllr", SqlDbType.VarChar,30),
					new SqlParameter("@lldh", SqlDbType.VarChar,50),
					new SqlParameter("@xp", SqlDbType.VarChar,100),
					new SqlParameter("@gz1", SqlDbType.VarChar,100),
					new SqlParameter("@gz2", SqlDbType.VarChar,100),
					new SqlParameter("@gz3", SqlDbType.VarChar,100),
					new SqlParameter("@gz4", SqlDbType.VarChar,100),
					new SqlParameter("@gz5", SqlDbType.VarChar,100),
					new SqlParameter("@sfxyg", SqlDbType.Bit,1),
					new SqlParameter("@ygzt", SqlDbType.Bit,1),
					new SqlParameter("@sgz", SqlDbType.VarChar,50),
					new SqlParameter("@jndj1", SqlDbType.VarChar,50),
					new SqlParameter("@jndj2", SqlDbType.VarChar,50),
					new SqlParameter("@jndj3", SqlDbType.VarChar,50),
					new SqlParameter("@createtime", SqlDbType.DateTime),
					new SqlParameter("@htbh", SqlDbType.VarChar,50),
					new SqlParameter("@xx", SqlDbType.VarChar,50),
					new SqlParameter("@aqjycj", SqlDbType.VarChar,50),
					new SqlParameter("@htyjpath", SqlDbType.VarChar,50),
					new SqlParameter("@intime", SqlDbType.DateTime),
					new SqlParameter("@outtime", SqlDbType.DateTime)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.xm;
			parameters[2].Value = model.xb;
			parameters[3].Value = model.jg;
			parameters[4].Value = model.gh;
			parameters[5].Value = model.csrq;
			parameters[6].Value = model.mz;
			parameters[7].Value = model.ywpx;
			parameters[8].Value = model.xl;
			parameters[9].Value = model.zz;
			parameters[10].Value = model.lxdh;
			parameters[11].Value = model.jjllr;
			parameters[12].Value = model.lldh;
			parameters[13].Value = model.xp;
			parameters[14].Value = model.gz1;
			parameters[15].Value = model.gz2;
			parameters[16].Value = model.gz3;
			parameters[17].Value = model.gz4;
			parameters[18].Value = model.gz5;
			parameters[19].Value = model.sfxyg;
			parameters[20].Value = model.ygzt;
			parameters[21].Value = model.sgz;
			parameters[22].Value = model.jndj1;
			parameters[23].Value = model.jndj2;
			parameters[24].Value = model.jndj3;
			parameters[25].Value = model.createtime;
			parameters[26].Value = model.htbh;
			parameters[27].Value = model.xx;
			parameters[28].Value = model.aqjycj;
			parameters[29].Value = model.htyjpath;
			parameters[30].Value = model.intime;
			parameters[31].Value = model.outtime;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Ry_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ry_Info set ");
			strSql.Append("xm=@xm,");
			strSql.Append("xb=@xb,");
			strSql.Append("jg=@jg,");
			strSql.Append("gh=@gh,");
			strSql.Append("csrq=@csrq,");
			strSql.Append("mz=@mz,");
			strSql.Append("ywpx=@ywpx,");
			strSql.Append("xl=@xl,");
			strSql.Append("zz=@zz,");
			strSql.Append("lxdh=@lxdh,");
			strSql.Append("jjllr=@jjllr,");
			strSql.Append("lldh=@lldh,");
			strSql.Append("xp=@xp,");
			strSql.Append("gz1=@gz1,");
			strSql.Append("gz2=@gz2,");
			strSql.Append("gz3=@gz3,");
			strSql.Append("gz4=@gz4,");
			strSql.Append("gz5=@gz5,");
			strSql.Append("sfxyg=@sfxyg,");
			strSql.Append("ygzt=@ygzt,");
			strSql.Append("sgz=@sgz,");
			strSql.Append("jndj1=@jndj1,");
			strSql.Append("jndj2=@jndj2,");
			strSql.Append("jndj3=@jndj3,");
			strSql.Append("createtime=@createtime,");
			strSql.Append("htbh=@htbh,");
			strSql.Append("xx=@xx,");
			strSql.Append("aqjycj=@aqjycj,");
			strSql.Append("htyjpath=@htyjpath,");
			strSql.Append("intime=@intime,");
			strSql.Append("outtime=@outtime");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@xb", SqlDbType.Bit,1),
					new SqlParameter("@jg", SqlDbType.VarChar,15),
					new SqlParameter("@gh", SqlDbType.VarChar,20),
					new SqlParameter("@csrq", SqlDbType.DateTime),
					new SqlParameter("@mz", SqlDbType.VarChar,10),
					new SqlParameter("@ywpx", SqlDbType.Bit,1),
					new SqlParameter("@xl", SqlDbType.VarChar,10),
					new SqlParameter("@zz", SqlDbType.VarChar,300),
					new SqlParameter("@lxdh", SqlDbType.VarChar,50),
					new SqlParameter("@jjllr", SqlDbType.VarChar,30),
					new SqlParameter("@lldh", SqlDbType.VarChar,50),
					new SqlParameter("@xp", SqlDbType.VarChar,100),
					new SqlParameter("@gz1", SqlDbType.VarChar,100),
					new SqlParameter("@gz2", SqlDbType.VarChar,100),
					new SqlParameter("@gz3", SqlDbType.VarChar,100),
					new SqlParameter("@gz4", SqlDbType.VarChar,100),
					new SqlParameter("@gz5", SqlDbType.VarChar,100),
					new SqlParameter("@sfxyg", SqlDbType.Bit,1),
					new SqlParameter("@ygzt", SqlDbType.Bit,1),
					new SqlParameter("@sgz", SqlDbType.VarChar,50),
					new SqlParameter("@jndj1", SqlDbType.VarChar,50),
					new SqlParameter("@jndj2", SqlDbType.VarChar,50),
					new SqlParameter("@jndj3", SqlDbType.VarChar,50),
					new SqlParameter("@createtime", SqlDbType.DateTime),
					new SqlParameter("@htbh", SqlDbType.VarChar,50),
					new SqlParameter("@xx", SqlDbType.VarChar,50),
					new SqlParameter("@aqjycj", SqlDbType.VarChar,50),
					new SqlParameter("@htyjpath", SqlDbType.VarChar,50),
					new SqlParameter("@intime", SqlDbType.DateTime),
					new SqlParameter("@outtime", SqlDbType.DateTime),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)};
			parameters[0].Value = model.xm;
			parameters[1].Value = model.xb;
			parameters[2].Value = model.jg;
			parameters[3].Value = model.gh;
			parameters[4].Value = model.csrq;
			parameters[5].Value = model.mz;
			parameters[6].Value = model.ywpx;
			parameters[7].Value = model.xl;
			parameters[8].Value = model.zz;
			parameters[9].Value = model.lxdh;
			parameters[10].Value = model.jjllr;
			parameters[11].Value = model.lldh;
			parameters[12].Value = model.xp;
			parameters[13].Value = model.gz1;
			parameters[14].Value = model.gz2;
			parameters[15].Value = model.gz3;
			parameters[16].Value = model.gz4;
			parameters[17].Value = model.gz5;
			parameters[18].Value = model.sfxyg;
			parameters[19].Value = model.ygzt;
			parameters[20].Value = model.sgz;
			parameters[21].Value = model.jndj1;
			parameters[22].Value = model.jndj2;
			parameters[23].Value = model.jndj3;
			parameters[24].Value = model.createtime;
			parameters[25].Value = model.htbh;
			parameters[26].Value = model.xx;
			parameters[27].Value = model.aqjycj;
			parameters[28].Value = model.htyjpath;
			parameters[29].Value = model.intime;
			parameters[30].Value = model.outtime;
			parameters[31].Value = model.sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ry_Info ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string sfzhlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Ry_Info ");
			strSql.Append(" where sfzh in ("+sfzhlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Ry_Info GetModel(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sfzh,xm,xb,jg,gh,csrq,mz,ywpx,xl,zz,lxdh,jjllr,lldh,xp,gz1,gz2,gz3,gz4,gz5,sfxyg,ygzt,sgz,jndj1,jndj2,jndj3,createtime,htbh,xx,aqjycj,htyjpath,intime,outtime from Ry_Info ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			Maticsoft.Model.Ry_Info model=new Maticsoft.Model.Ry_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Ry_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Ry_Info model=new Maticsoft.Model.Ry_Info();
			if (row != null)
			{
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["xm"]!=null)
				{
					model.xm=row["xm"].ToString();
				}
				if(row["xb"]!=null && row["xb"].ToString()!="")
				{
					if((row["xb"].ToString()=="1")||(row["xb"].ToString().ToLower()=="true"))
					{
						model.xb=true;
					}
					else
					{
						model.xb=false;
					}
				}
				if(row["jg"]!=null)
				{
					model.jg=row["jg"].ToString();
				}
				if(row["gh"]!=null)
				{
					model.gh=row["gh"].ToString();
				}
				if(row["csrq"]!=null && row["csrq"].ToString()!="")
				{
					model.csrq=DateTime.Parse(row["csrq"].ToString());
				}
				if(row["mz"]!=null)
				{
					model.mz=row["mz"].ToString();
				}
				if(row["ywpx"]!=null && row["ywpx"].ToString()!="")
				{
					if((row["ywpx"].ToString()=="1")||(row["ywpx"].ToString().ToLower()=="true"))
					{
						model.ywpx=true;
					}
					else
					{
						model.ywpx=false;
					}
				}
				if(row["xl"]!=null)
				{
					model.xl=row["xl"].ToString();
				}
				if(row["zz"]!=null)
				{
					model.zz=row["zz"].ToString();
				}
				if(row["lxdh"]!=null)
				{
					model.lxdh=row["lxdh"].ToString();
				}
				if(row["jjllr"]!=null)
				{
					model.jjllr=row["jjllr"].ToString();
				}
				if(row["lldh"]!=null)
				{
					model.lldh=row["lldh"].ToString();
				}
				if(row["xp"]!=null)
				{
					model.xp=row["xp"].ToString();
				}
				if(row["gz1"]!=null)
				{
					model.gz1=row["gz1"].ToString();
				}
				if(row["gz2"]!=null)
				{
					model.gz2=row["gz2"].ToString();
				}
				if(row["gz3"]!=null)
				{
					model.gz3=row["gz3"].ToString();
				}
				if(row["gz4"]!=null)
				{
					model.gz4=row["gz4"].ToString();
				}
				if(row["gz5"]!=null)
				{
					model.gz5=row["gz5"].ToString();
				}
				if(row["sfxyg"]!=null && row["sfxyg"].ToString()!="")
				{
					if((row["sfxyg"].ToString()=="1")||(row["sfxyg"].ToString().ToLower()=="true"))
					{
						model.sfxyg=true;
					}
					else
					{
						model.sfxyg=false;
					}
				}
				if(row["ygzt"]!=null && row["ygzt"].ToString()!="")
				{
					if((row["ygzt"].ToString()=="1")||(row["ygzt"].ToString().ToLower()=="true"))
					{
						model.ygzt=true;
					}
					else
					{
						model.ygzt=false;
					}
				}
				if(row["sgz"]!=null)
				{
					model.sgz=row["sgz"].ToString();
				}
				if(row["jndj1"]!=null)
				{
					model.jndj1=row["jndj1"].ToString();
				}
				if(row["jndj2"]!=null)
				{
					model.jndj2=row["jndj2"].ToString();
				}
				if(row["jndj3"]!=null)
				{
					model.jndj3=row["jndj3"].ToString();
				}
				if(row["createtime"]!=null && row["createtime"].ToString()!="")
				{
					model.createtime=DateTime.Parse(row["createtime"].ToString());
				}
				if(row["htbh"]!=null)
				{
					model.htbh=row["htbh"].ToString();
				}
				if(row["xx"]!=null)
				{
					model.xx=row["xx"].ToString();
				}
				if(row["aqjycj"]!=null)
				{
					model.aqjycj=row["aqjycj"].ToString();
				}
				if(row["htyjpath"]!=null)
				{
					model.htyjpath=row["htyjpath"].ToString();
				}
				if(row["intime"]!=null && row["intime"].ToString()!="")
				{
					model.intime=DateTime.Parse(row["intime"].ToString());
				}
				if(row["outtime"]!=null && row["outtime"].ToString()!="")
				{
					model.outtime=DateTime.Parse(row["outtime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select id=ROW_NUMBER()over(order by intime desc), sfzh,xm,case(xb) when 1 then '男' else '女' end as xb,jg,gh,csrq,mz,ywpx,xl,zz,lxdh,jjllr,lldh,xp,gz1,gz2,gz3,gz4,gz5,sfxyg,ygzt,sgz,jndj1,jndj2,jndj3,createtime,htbh,xx,aqjycj,htyjpath,intime,outtime ");
			strSql.Append(" FROM Ry_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 获得数据列表by住宿人员添加
        /// </summary>
        public DataSet GetListByStay(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("   select xh=ROW_NUMBER()over(order by a.createtime desc),a.sfzh, a.xm,gz=a.gz1,b.bzid,bz=c.bzmc,a.gh,xb=case when a.xb=0 then '女' else '男' end ,a.csrq from Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid  where a.sfzh not in (select sfzh from Ry_Stay_Info) ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(  strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sfzh,xm,xb,jg,gh,csrq,mz,ywpx,xl,zz,lxdh,jjllr,lldh,xp,gz1,gz2,gz3,gz4,gz5,sfxyg,ygzt,sgz,jndj1,jndj2,jndj3,createtime,htbh,xx,aqjycj,htyjpath,intime,outtime ");
			strSql.Append(" FROM Ry_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Ry_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sfzh desc");
			}
			strSql.Append(")AS Row, T.*  from Ry_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ry_Info";
			parameters[1].Value = "sfzh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

