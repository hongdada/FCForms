﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:RyPrint_Info
	/// </summary>
	public partial class RyPrint_Info
	{
		public RyPrint_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string sfzh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from RyPrint_Info");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.RyPrint_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into RyPrint_Info(");
			strSql.Append("sfzh,zw1,zw2,zw3,zw4,zw5,zw6,zw7,zw8,zw9,zw10,gly)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@zw1,@zw2,@zw3,@zw4,@zw5,@zw6,@zw7,@zw8,@zw9,@zw10,@gly)");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@zw1", SqlDbType.VarChar,3000),
					new SqlParameter("@zw2", SqlDbType.VarChar,3000),
					new SqlParameter("@zw3", SqlDbType.VarChar,3000),
					new SqlParameter("@zw4", SqlDbType.VarChar,3000),
					new SqlParameter("@zw5", SqlDbType.VarChar,3000),
					new SqlParameter("@zw6", SqlDbType.VarChar,3000),
					new SqlParameter("@zw7", SqlDbType.VarChar,3000),
					new SqlParameter("@zw8", SqlDbType.VarChar,3000),
					new SqlParameter("@zw9", SqlDbType.VarChar,3000),
					new SqlParameter("@zw10", SqlDbType.VarChar,3000),
					new SqlParameter("@gly", SqlDbType.Bit,1)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.zw1;
			parameters[2].Value = model.zw2;
			parameters[3].Value = model.zw3;
			parameters[4].Value = model.zw4;
			parameters[5].Value = model.zw5;
			parameters[6].Value = model.zw6;
			parameters[7].Value = model.zw7;
			parameters[8].Value = model.zw8;
			parameters[9].Value = model.zw9;
			parameters[10].Value = model.zw10;
			parameters[11].Value = model.gly;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.RyPrint_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update RyPrint_Info set ");
			strSql.Append("zw1=@zw1,");
			strSql.Append("zw2=@zw2,");
			strSql.Append("zw3=@zw3,");
			strSql.Append("zw4=@zw4,");
			strSql.Append("zw5=@zw5,");
			strSql.Append("zw6=@zw6,");
			strSql.Append("zw7=@zw7,");
			strSql.Append("zw8=@zw8,");
			strSql.Append("zw9=@zw9,");
			strSql.Append("zw10=@zw10,");
			strSql.Append("gly=@gly");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@zw1", SqlDbType.VarChar,3000),
					new SqlParameter("@zw2", SqlDbType.VarChar,3000),
					new SqlParameter("@zw3", SqlDbType.VarChar,3000),
					new SqlParameter("@zw4", SqlDbType.VarChar,3000),
					new SqlParameter("@zw5", SqlDbType.VarChar,3000),
					new SqlParameter("@zw6", SqlDbType.VarChar,3000),
					new SqlParameter("@zw7", SqlDbType.VarChar,3000),
					new SqlParameter("@zw8", SqlDbType.VarChar,3000),
					new SqlParameter("@zw9", SqlDbType.VarChar,3000),
					new SqlParameter("@zw10", SqlDbType.VarChar,3000),
					new SqlParameter("@gly", SqlDbType.Bit,1),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)};
			parameters[0].Value = model.zw1;
			parameters[1].Value = model.zw2;
			parameters[2].Value = model.zw3;
			parameters[3].Value = model.zw4;
			parameters[4].Value = model.zw5;
			parameters[5].Value = model.zw6;
			parameters[6].Value = model.zw7;
			parameters[7].Value = model.zw8;
			parameters[8].Value = model.zw9;
			parameters[9].Value = model.zw10;
			parameters[10].Value = model.gly;
			parameters[11].Value = model.sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyPrint_Info ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string sfzhlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyPrint_Info ");
			strSql.Append(" where sfzh in ("+sfzhlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyPrint_Info GetModel(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sfzh,zw1,zw2,zw3,zw4,zw5,zw6,zw7,zw8,zw9,zw10,gly from RyPrint_Info ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			Maticsoft.Model.RyPrint_Info model=new Maticsoft.Model.RyPrint_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyPrint_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.RyPrint_Info model=new Maticsoft.Model.RyPrint_Info();
			if (row != null)
			{
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["zw1"]!=null)
				{
					model.zw1=row["zw1"].ToString();
				}
				if(row["zw2"]!=null)
				{
					model.zw2=row["zw2"].ToString();
				}
				if(row["zw3"]!=null)
				{
					model.zw3=row["zw3"].ToString();
				}
				if(row["zw4"]!=null)
				{
					model.zw4=row["zw4"].ToString();
				}
				if(row["zw5"]!=null)
				{
					model.zw5=row["zw5"].ToString();
				}
				if(row["zw6"]!=null)
				{
					model.zw6=row["zw6"].ToString();
				}
				if(row["zw7"]!=null)
				{
					model.zw7=row["zw7"].ToString();
				}
				if(row["zw8"]!=null)
				{
					model.zw8=row["zw8"].ToString();
				}
				if(row["zw9"]!=null)
				{
					model.zw9=row["zw9"].ToString();
				}
				if(row["zw10"]!=null)
				{
					model.zw10=row["zw10"].ToString();
				}
				if(row["gly"]!=null && row["gly"].ToString()!="")
				{
					if((row["gly"].ToString()=="1")||(row["gly"].ToString().ToLower()=="true"))
					{
						model.gly=true;
					}
					else
					{
						model.gly=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sfzh,zw1,zw2,zw3,zw4,zw5,zw6,zw7,zw8,zw9,zw10,gly ");
			strSql.Append(" FROM RyPrint_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sfzh,zw1,zw2,zw3,zw4,zw5,zw6,zw7,zw8,zw9,zw10,gly ");
			strSql.Append(" FROM RyPrint_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM RyPrint_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sfzh desc");
			}
			strSql.Append(")AS Row, T.*  from RyPrint_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "RyPrint_Info";
			parameters[1].Value = "sfzh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

