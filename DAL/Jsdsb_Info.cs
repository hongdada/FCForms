﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Jsdsb_Info
	/// </summary>
	public partial class Jsdsb_Info
	{
		public Jsdsb_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("jsdsblsh", "Jsdsb_Info"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int jsdsblsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Jsdsb_Info");
			strSql.Append(" where jsdsblsh=@jsdsblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4)
			};
			parameters[0].Value = jsdsblsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Jsdsb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Jsdsb_Info(");
			strSql.Append("jsdbh,sblsh,sfkqkq,sfkqjs)");
			strSql.Append(" values (");
			strSql.Append("@jsdbh,@sblsh,@sfkqkq,@sfkqjs)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sblsh", SqlDbType.SmallInt,2),
					new SqlParameter("@sfkqkq", SqlDbType.Bit,1),
					new SqlParameter("@sfkqjs", SqlDbType.Bit,1)};
			parameters[0].Value = model.jsdbh;
			parameters[1].Value = model.sblsh;
			parameters[2].Value = model.sfkqkq;
			parameters[3].Value = model.sfkqjs;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Jsdsb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Jsdsb_Info set ");
			strSql.Append("jsdbh=@jsdbh,");
			strSql.Append("sblsh=@sblsh,");
			strSql.Append("sfkqkq=@sfkqkq,");
			strSql.Append("sfkqjs=@sfkqjs");
			strSql.Append(" where jsdsblsh=@jsdsblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sblsh", SqlDbType.SmallInt,2),
					new SqlParameter("@sfkqkq", SqlDbType.Bit,1),
					new SqlParameter("@sfkqjs", SqlDbType.Bit,1),
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4)};
			parameters[0].Value = model.jsdbh;
			parameters[1].Value = model.sblsh;
			parameters[2].Value = model.sfkqkq;
			parameters[3].Value = model.sfkqjs;
			parameters[4].Value = model.jsdsblsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int jsdsblsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Jsdsb_Info ");
			strSql.Append(" where jsdsblsh=@jsdsblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4)
			};
			parameters[0].Value = jsdsblsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string jsdsblshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Jsdsb_Info ");
			strSql.Append(" where jsdsblsh in ("+jsdsblshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Jsdsb_Info GetModel(int jsdsblsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 jsdsblsh,jsdbh,sblsh,sfkqkq,sfkqjs from Jsdsb_Info ");
			strSql.Append(" where jsdsblsh=@jsdsblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4)
			};
			parameters[0].Value = jsdsblsh;

			Maticsoft.Model.Jsdsb_Info model=new Maticsoft.Model.Jsdsb_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Jsdsb_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Jsdsb_Info model=new Maticsoft.Model.Jsdsb_Info();
			if (row != null)
			{
				if(row["jsdsblsh"]!=null && row["jsdsblsh"].ToString()!="")
				{
					model.jsdsblsh=int.Parse(row["jsdsblsh"].ToString());
				}
				if(row["jsdbh"]!=null)
				{
					model.jsdbh=row["jsdbh"].ToString();
				}
				if(row["sblsh"]!=null && row["sblsh"].ToString()!="")
				{
					model.sblsh=int.Parse(row["sblsh"].ToString());
				}
				if(row["sfkqkq"]!=null && row["sfkqkq"].ToString()!="")
				{
					if((row["sfkqkq"].ToString()=="1")||(row["sfkqkq"].ToString().ToLower()=="true"))
					{
						model.sfkqkq=true;
					}
					else
					{
						model.sfkqkq=false;
					}
				}
				if(row["sfkqjs"]!=null && row["sfkqjs"].ToString()!="")
				{
					if((row["sfkqjs"].ToString()=="1")||(row["sfkqjs"].ToString().ToLower()=="true"))
					{
						model.sfkqjs=true;
					}
					else
					{
						model.sfkqjs=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select jsdsblsh,jsdbh,sblsh,sfkqkq,sfkqjs ");
			strSql.Append(" FROM Jsdsb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" jsdsblsh,jsdbh,sblsh,sfkqkq,sfkqjs ");
			strSql.Append(" FROM Jsdsb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Jsdsb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.jsdsblsh desc");
			}
			strSql.Append(")AS Row, T.*  from Jsdsb_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Jsdsb_Info";
			parameters[1].Value = "jsdsblsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

