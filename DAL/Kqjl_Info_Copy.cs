﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Kqjl_Info_Copy
	/// </summary>
	public partial class Kqjl_Info_Copy
	{
		public Kqjl_Info_Copy()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long kqlsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Kqjl_Info_Copy");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(Maticsoft.Model.Kqjl_Info_Copy model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Kqjl_Info_Copy(");
			strSql.Append("jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff)");
			strSql.Append(" values (");
			strSql.Append("@jsdm,@sfzh,@xm,@qybmm,@gz,@fbm,@kh,@kbs,@kqsj,@jczt,@sfdj,@kqlx,@yyxp,@zpxp,@sfff)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@gz", SqlDbType.VarChar,100),
					new SqlParameter("@fbm", SqlDbType.VarChar,100),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@kqsj", SqlDbType.DateTime),
					new SqlParameter("@jczt", SqlDbType.Bit,1),
					new SqlParameter("@sfdj", SqlDbType.Bit,1),
					new SqlParameter("@kqlx", SqlDbType.VarChar,50),
					new SqlParameter("@yyxp", SqlDbType.VarChar,100),
					new SqlParameter("@zpxp", SqlDbType.VarChar,100),
					new SqlParameter("@sfff", SqlDbType.Bit,1)};
			parameters[0].Value = model.jsdm;
			parameters[1].Value = model.sfzh;
			parameters[2].Value = model.xm;
			parameters[3].Value = model.qybmm;
			parameters[4].Value = model.gz;
			parameters[5].Value = model.fbm;
			parameters[6].Value = model.kh;
			parameters[7].Value = model.kbs;
			parameters[8].Value = model.kqsj;
			parameters[9].Value = model.jczt;
			parameters[10].Value = model.sfdj;
			parameters[11].Value = model.kqlx;
			parameters[12].Value = model.yyxp;
			parameters[13].Value = model.zpxp;
			parameters[14].Value = model.sfff;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt64(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Kqjl_Info_Copy model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Kqjl_Info_Copy set ");
			strSql.Append("jsdm=@jsdm,");
			strSql.Append("sfzh=@sfzh,");
			strSql.Append("xm=@xm,");
			strSql.Append("qybmm=@qybmm,");
			strSql.Append("gz=@gz,");
			strSql.Append("fbm=@fbm,");
			strSql.Append("kh=@kh,");
			strSql.Append("kbs=@kbs,");
			strSql.Append("kqsj=@kqsj,");
			strSql.Append("jczt=@jczt,");
			strSql.Append("sfdj=@sfdj,");
			strSql.Append("kqlx=@kqlx,");
			strSql.Append("yyxp=@yyxp,");
			strSql.Append("zpxp=@zpxp,");
			strSql.Append("sfff=@sfff");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@gz", SqlDbType.VarChar,100),
					new SqlParameter("@fbm", SqlDbType.VarChar,100),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@kqsj", SqlDbType.DateTime),
					new SqlParameter("@jczt", SqlDbType.Bit,1),
					new SqlParameter("@sfdj", SqlDbType.Bit,1),
					new SqlParameter("@kqlx", SqlDbType.VarChar,50),
					new SqlParameter("@yyxp", SqlDbType.VarChar,100),
					new SqlParameter("@zpxp", SqlDbType.VarChar,100),
					new SqlParameter("@sfff", SqlDbType.Bit,1),
					new SqlParameter("@kqlsh", SqlDbType.BigInt,8)};
			parameters[0].Value = model.jsdm;
			parameters[1].Value = model.sfzh;
			parameters[2].Value = model.xm;
			parameters[3].Value = model.qybmm;
			parameters[4].Value = model.gz;
			parameters[5].Value = model.fbm;
			parameters[6].Value = model.kh;
			parameters[7].Value = model.kbs;
			parameters[8].Value = model.kqsj;
			parameters[9].Value = model.jczt;
			parameters[10].Value = model.sfdj;
			parameters[11].Value = model.kqlx;
			parameters[12].Value = model.yyxp;
			parameters[13].Value = model.zpxp;
			parameters[14].Value = model.sfff;
			parameters[15].Value = model.kqlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long kqlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Kqjl_Info_Copy ");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string kqlshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Kqjl_Info_Copy ");
			strSql.Append(" where kqlsh in ("+kqlshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Kqjl_Info_Copy GetModel(long kqlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 kqlsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff from Kqjl_Info_Copy ");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			Maticsoft.Model.Kqjl_Info_Copy model=new Maticsoft.Model.Kqjl_Info_Copy();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Kqjl_Info_Copy DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Kqjl_Info_Copy model=new Maticsoft.Model.Kqjl_Info_Copy();
			if (row != null)
			{
				if(row["kqlsh"]!=null && row["kqlsh"].ToString()!="")
				{
					model.kqlsh=long.Parse(row["kqlsh"].ToString());
				}
				if(row["jsdm"]!=null)
				{
					model.jsdm=row["jsdm"].ToString();
				}
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["xm"]!=null)
				{
					model.xm=row["xm"].ToString();
				}
				if(row["qybmm"]!=null)
				{
					model.qybmm=row["qybmm"].ToString();
				}
				if(row["gz"]!=null)
				{
					model.gz=row["gz"].ToString();
				}
				if(row["fbm"]!=null)
				{
					model.fbm=row["fbm"].ToString();
				}
				if(row["kh"]!=null)
				{
					model.kh=row["kh"].ToString();
				}
				if(row["kbs"]!=null)
				{
					model.kbs=row["kbs"].ToString();
				}
				if(row["kqsj"]!=null && row["kqsj"].ToString()!="")
				{
					model.kqsj=DateTime.Parse(row["kqsj"].ToString());
				}
				if(row["jczt"]!=null && row["jczt"].ToString()!="")
				{
					if((row["jczt"].ToString()=="1")||(row["jczt"].ToString().ToLower()=="true"))
					{
						model.jczt=true;
					}
					else
					{
						model.jczt=false;
					}
				}
				if(row["sfdj"]!=null && row["sfdj"].ToString()!="")
				{
					if((row["sfdj"].ToString()=="1")||(row["sfdj"].ToString().ToLower()=="true"))
					{
						model.sfdj=true;
					}
					else
					{
						model.sfdj=false;
					}
				}
				if(row["kqlx"]!=null)
				{
					model.kqlx=row["kqlx"].ToString();
				}
				if(row["yyxp"]!=null)
				{
					model.yyxp=row["yyxp"].ToString();
				}
				if(row["zpxp"]!=null)
				{
					model.zpxp=row["zpxp"].ToString();
				}
				if(row["sfff"]!=null && row["sfff"].ToString()!="")
				{
					if((row["sfff"].ToString()=="1")||(row["sfff"].ToString().ToLower()=="true"))
					{
						model.sfff=true;
					}
					else
					{
						model.sfff=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select kqlsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff ");
			strSql.Append(" FROM Kqjl_Info_Copy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListByTopOne(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("  select top 1 a.kqlsh,a.jsdm,a.sfzh,a.xm,a.qybmm,a.gz,a.fbm,a.kh,a.kbs,a.kqsj,a.jczt,a.sfdj,a.kqlx,a.yyxp,a.zpxp,a.sfff,c.fbmc,d.gh from Kqjl_Info_Copy(nolock) a left join Fenbry_Info(nolock) b on a.sfzh=b.sfzh left join Fenb_Info(nolock) c on b.fbid=c.fbid join Ry_Info(nolock) d on a.sfzh=d.sfzh ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListBySplitScreenStatistics(string kqdm)
        {
            StringBuilder strSql = new StringBuilder();
            string strWhere = string.Empty;
            if (!string.IsNullOrEmpty(kqdm))
            {
                strWhere = "and jsdm='" + kqdm + "'";
            }
            strSql.Append("   with tb as ( select  ROW_NUMBER ()over(partition by xm order by kqsj desc) num,* from Kqjl_Info_Copy ) " +
  " select bz='',CountNum=(select COUNT(1) from tb where jczt=1 and num=1 " + strWhere + ")" +
  " union all select bz=qybmm,CountNum=COUNT(1) from tb where jczt=1 and num=1 " + strWhere + " group by qybmm ");
            return DbHelperSQL.Query(strSql.ToString());
        }
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" kqlsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff ");
			strSql.Append(" FROM Kqjl_Info_Copy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Kqjl_Info_Copy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.kqlsh desc");
			}
			strSql.Append(")AS Row, T.*  from Kqjl_Info_Copy T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Kqjl_Info_Copy";
			parameters[1].Value = "kqlsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

