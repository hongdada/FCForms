﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Plugin_CSCECTWO_Record
	/// </summary>
	public partial class Plugin_CSCECTWO_Record
	{
		public Plugin_CSCECTWO_Record()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string sfzh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Plugin_CSCECTWO_Record");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.Plugin_CSCECTWO_Record model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Plugin_CSCECTWO_Record(");
			strSql.Append("sfzh,sjjysj,jccnssj,tccnssj,ldhtssj)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@sjjysj,@jccnssj,@tccnssj,@ldhtssj)");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@sjjysj", SqlDbType.DateTime),
					new SqlParameter("@jccnssj", SqlDbType.DateTime),
					new SqlParameter("@tccnssj", SqlDbType.DateTime),
					new SqlParameter("@ldhtssj", SqlDbType.DateTime)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.sjjysj;
			parameters[2].Value = model.jccnssj;
			parameters[3].Value = model.tccnssj;
			parameters[4].Value = model.ldhtssj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Plugin_CSCECTWO_Record model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Plugin_CSCECTWO_Record set ");
			strSql.Append("sjjysj=@sjjysj,");
			strSql.Append("jccnssj=@jccnssj,");
			strSql.Append("tccnssj=@tccnssj,");
			strSql.Append("ldhtssj=@ldhtssj");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sjjysj", SqlDbType.DateTime),
					new SqlParameter("@jccnssj", SqlDbType.DateTime),
					new SqlParameter("@tccnssj", SqlDbType.DateTime),
					new SqlParameter("@ldhtssj", SqlDbType.DateTime),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)};
			parameters[0].Value = model.sjjysj;
			parameters[1].Value = model.jccnssj;
			parameters[2].Value = model.tccnssj;
			parameters[3].Value = model.ldhtssj;
			parameters[4].Value = model.sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Plugin_CSCECTWO_Record ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string sfzhlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Plugin_CSCECTWO_Record ");
			strSql.Append(" where sfzh in ("+sfzhlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Plugin_CSCECTWO_Record GetModel(string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sfzh,sjjysj,jccnssj,tccnssj,ldhtssj from Plugin_CSCECTWO_Record ");
			strSql.Append(" where sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = sfzh;

			Maticsoft.Model.Plugin_CSCECTWO_Record model=new Maticsoft.Model.Plugin_CSCECTWO_Record();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Plugin_CSCECTWO_Record DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Plugin_CSCECTWO_Record model=new Maticsoft.Model.Plugin_CSCECTWO_Record();
			if (row != null)
			{
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["sjjysj"]!=null && row["sjjysj"].ToString()!="")
				{
					model.sjjysj=DateTime.Parse(row["sjjysj"].ToString());
				}
				if(row["jccnssj"]!=null && row["jccnssj"].ToString()!="")
				{
					model.jccnssj=DateTime.Parse(row["jccnssj"].ToString());
				}
				if(row["tccnssj"]!=null && row["tccnssj"].ToString()!="")
				{
					model.tccnssj=DateTime.Parse(row["tccnssj"].ToString());
				}
				if(row["ldhtssj"]!=null && row["ldhtssj"].ToString()!="")
				{
					model.ldhtssj=DateTime.Parse(row["ldhtssj"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sfzh,sjjysj,jccnssj,tccnssj,ldhtssj ");
			strSql.Append(" FROM Plugin_CSCECTWO_Record ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sfzh,sjjysj,jccnssj,tccnssj,ldhtssj ");
			strSql.Append(" FROM Plugin_CSCECTWO_Record ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Plugin_CSCECTWO_Record ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sfzh desc");
			}
			strSql.Append(")AS Row, T.*  from Plugin_CSCECTWO_Record T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Plugin_CSCECTWO_Record";
			parameters[1].Value = "sfzh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

