﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:RyCa_Info
	/// </summary>
	public partial class RyCa_Info
	{
		public RyCa_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long lsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from RyCa_Info");
			strSql.Append(" where lsh=@lsh");
			SqlParameter[] parameters = {
					new SqlParameter("@lsh", SqlDbType.BigInt)
			};
			parameters[0].Value = lsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(Maticsoft.Model.RyCa_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into RyCa_Info(");
			strSql.Append("sfzh,kh,fksj,cxsj)");
			strSql.Append(" values (");
			strSql.Append("@sfzh,@kh,@fksj,@cxsj)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@fksj", SqlDbType.DateTime),
					new SqlParameter("@cxsj", SqlDbType.DateTime)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.kh;
			parameters[2].Value = model.fksj;
			parameters[3].Value = model.cxsj;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt64(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.RyCa_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update RyCa_Info set ");
			strSql.Append("sfzh=@sfzh,");
			strSql.Append("kh=@kh,");
			strSql.Append("fksj=@fksj,");
			strSql.Append("cxsj=@cxsj");
			strSql.Append(" where lsh=@lsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@fksj", SqlDbType.DateTime),
					new SqlParameter("@cxsj", SqlDbType.DateTime),
					new SqlParameter("@lsh", SqlDbType.BigInt,8)};
			parameters[0].Value = model.sfzh;
			parameters[1].Value = model.kh;
			parameters[2].Value = model.fksj;
			parameters[3].Value = model.cxsj;
			parameters[4].Value = model.lsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long lsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyCa_Info ");
			strSql.Append(" where lsh=@lsh");
			SqlParameter[] parameters = {
					new SqlParameter("@lsh", SqlDbType.BigInt)
			};
			parameters[0].Value = lsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string lshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from RyCa_Info ");
			strSql.Append(" where lsh in ("+lshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyCa_Info GetModel(long lsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 lsh,sfzh,kh,fksj,cxsj from RyCa_Info ");
			strSql.Append(" where lsh=@lsh");
			SqlParameter[] parameters = {
					new SqlParameter("@lsh", SqlDbType.BigInt)
			};
			parameters[0].Value = lsh;

			Maticsoft.Model.RyCa_Info model=new Maticsoft.Model.RyCa_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.RyCa_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.RyCa_Info model=new Maticsoft.Model.RyCa_Info();
			if (row != null)
			{
				if(row["lsh"]!=null && row["lsh"].ToString()!="")
				{
					model.lsh=long.Parse(row["lsh"].ToString());
				}
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["kh"]!=null)
				{
					model.kh=row["kh"].ToString();
				}
				if(row["fksj"]!=null && row["fksj"].ToString()!="")
				{
					model.fksj=DateTime.Parse(row["fksj"].ToString());
				}
				if(row["cxsj"]!=null && row["cxsj"].ToString()!="")
				{
					model.cxsj=DateTime.Parse(row["cxsj"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select lsh,sfzh,kh,fksj,cxsj ");
			strSql.Append(" FROM RyCa_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" lsh,sfzh,kh,fksj,cxsj ");
			strSql.Append(" FROM RyCa_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM RyCa_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.lsh desc");
			}
			strSql.Append(")AS Row, T.*  from RyCa_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "RyCa_Info";
			parameters[1].Value = "lsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

