using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//请先添加引用
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类Ry_Stay_Info。
	/// </summary>
	public class Ry_Stay_Info
	{
		public Ry_Stay_Info()
		{}
		#region  成员方法

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "Ry_Stay_Info"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Ry_Stay_Info");
			strSql.Append(" where ID=@ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Ry_Stay_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Ry_Stay_Info(");
			strSql.Append("xm,sfzh,state,wza,wzb,CreateTime,LastTime)");
			strSql.Append(" values (");
			strSql.Append("@xm,@sfzh,@state,@wza,@wzb,@CreateTime,@LastTime)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@xm", SqlDbType.NVarChar,20),
					new SqlParameter("@sfzh", SqlDbType.NVarChar,50),
					new SqlParameter("@state", SqlDbType.TinyInt,1),
					new SqlParameter("@wza", SqlDbType.NVarChar,50),
					new SqlParameter("@wzb", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@LastTime", SqlDbType.DateTime)};
			parameters[0].Value = model.xm;
			parameters[1].Value = model.sfzh;
			parameters[2].Value = model.state;
			parameters[3].Value = model.wza;
			parameters[4].Value = model.wzb;
			parameters[5].Value = model.CreateTime;
			parameters[6].Value = model.LastTime;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 1;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Ry_Stay_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Ry_Stay_Info set ");
			strSql.Append("xm=@xm,");
			strSql.Append("sfzh=@sfzh,");
			strSql.Append("state=@state,");
			strSql.Append("wza=@wza,");
			strSql.Append("wzb=@wzb,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("LastTime=@LastTime");
			strSql.Append(" where ID=@ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@xm", SqlDbType.NVarChar,20),
					new SqlParameter("@sfzh", SqlDbType.NVarChar,50),
					new SqlParameter("@state", SqlDbType.TinyInt,1),
					new SqlParameter("@wza", SqlDbType.NVarChar,50),
					new SqlParameter("@wzb", SqlDbType.NVarChar,50),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@LastTime", SqlDbType.DateTime)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.xm;
			parameters[2].Value = model.sfzh;
			parameters[3].Value = model.state;
			parameters[4].Value = model.wza;
			parameters[5].Value = model.wzb;
			parameters[6].Value = model.CreateTime;
			parameters[7].Value = model.LastTime;

			int num=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            if (num > 0) return true;
            else return false;
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool  Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete Ry_Stay_Info ");
			strSql.Append(" where ID=@ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = ID;

			int num=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            if (num > 0) return true;
            else return false;
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Ry_Stay_Info GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,xm,sfzh,state,wza,wzb,CreateTime,LastTime from Ry_Stay_Info ");
			strSql.Append(" where ID=@ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = ID;

			Maticsoft.Model.Ry_Stay_Info model=new Maticsoft.Model.Ry_Stay_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				model.xm=ds.Tables[0].Rows[0]["xm"].ToString();
				model.sfzh=ds.Tables[0].Rows[0]["sfzh"].ToString();
				if(ds.Tables[0].Rows[0]["state"].ToString()!="")
				{
					model.state=int.Parse(ds.Tables[0].Rows[0]["state"].ToString());
				}
				model.wza=ds.Tables[0].Rows[0]["wza"].ToString();
				model.wzb=ds.Tables[0].Rows[0]["wzb"].ToString();
				if(ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LastTime"].ToString()!="")
				{
					model.LastTime=DateTime.Parse(ds.Tables[0].Rows[0]["LastTime"].ToString());
				}
				return model;
			}
			else
			{
			return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,xm,sfzh,state,wza,wzb,CreateTime,LastTime ");
			strSql.Append(" FROM Ry_Stay_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListByStayInfo(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select xh=ROW_NUMBER()over(order by d.createtime desc), a.sfzh,a.xm,xb=case when a.xb=1 then '男' else '女' end,a.jg,a.gh,age=(DATEPART(YEAR,GETDATE())-DATEPART (year,a.csrq)),a.mz,gz=a.gz1,d.wza,d.wzb,wz=case when d.state=1 then d.wza+'#'+d.wzb else '' end,zt=case when d.state=1 then '在住' else '外住' end,a.intime,a.outtime,fh=case when d.state=1 then d.wza+'#'+d.wzb else '未入住' end,d.ID,c.bzid,bz=c.bzmc from Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join Ry_Stay_Info(nolock) d on a.sfzh=d.sfzh ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Ry_Stay_Info";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  成员方法
	}
}

