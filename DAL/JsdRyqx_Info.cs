﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:JsdRyqx_Info
	/// </summary>
	public partial class JsdRyqx_Info
	{
		public JsdRyqx_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string jsdbh,string sfzh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from JsdRyqx_Info");
			strSql.Append(" where jsdbh=@jsdbh and sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;
			parameters[1].Value = sfzh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Maticsoft.Model.JsdRyqx_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into JsdRyqx_Info(");
			strSql.Append("jsdbh,sfzh,kh,ryid,sfhbmd,sfkqfq,yxjcsj)");
			strSql.Append(" values (");
			strSql.Append("@jsdbh,@sfzh,@kh,@ryid,@sfhbmd,@sfkqfq,@yxjcsj)");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@ryid", SqlDbType.Int,4),
					new SqlParameter("@sfhbmd", SqlDbType.Bit,1),
					new SqlParameter("@sfkqfq", SqlDbType.Bit,1),
					new SqlParameter("@yxjcsj", SqlDbType.DateTime)};
			parameters[0].Value = model.jsdbh;
			parameters[1].Value = model.sfzh;
			parameters[2].Value = model.kh;
			parameters[3].Value = model.ryid;
			parameters[4].Value = model.sfhbmd;
			parameters[5].Value = model.sfkqfq;
			parameters[6].Value = model.yxjcsj;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.JsdRyqx_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update JsdRyqx_Info set ");
			strSql.Append("kh=@kh,");
			strSql.Append("ryid=@ryid,");
			strSql.Append("sfhbmd=@sfhbmd,");
			strSql.Append("sfkqfq=@sfkqfq,");
			strSql.Append("yxjcsj=@yxjcsj");
			strSql.Append(" where jsdbh=@jsdbh and sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@ryid", SqlDbType.Int,4),
					new SqlParameter("@sfhbmd", SqlDbType.Bit,1),
					new SqlParameter("@sfkqfq", SqlDbType.Bit,1),
					new SqlParameter("@yxjcsj", SqlDbType.DateTime),
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)};
			parameters[0].Value = model.kh;
			parameters[1].Value = model.ryid;
			parameters[2].Value = model.sfhbmd;
			parameters[3].Value = model.sfkqfq;
			parameters[4].Value = model.yxjcsj;
			parameters[5].Value = model.jsdbh;
			parameters[6].Value = model.sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string jsdbh,string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from JsdRyqx_Info ");
			strSql.Append(" where jsdbh=@jsdbh and sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;
			parameters[1].Value = sfzh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.JsdRyqx_Info GetModel(string jsdbh,string sfzh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 jsdbh,sfzh,kh,ryid,sfhbmd,sfkqfq,yxjcsj from JsdRyqx_Info ");
			strSql.Append(" where jsdbh=@jsdbh and sfzh=@sfzh ");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdbh", SqlDbType.VarChar,18),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18)			};
			parameters[0].Value = jsdbh;
			parameters[1].Value = sfzh;

			Maticsoft.Model.JsdRyqx_Info model=new Maticsoft.Model.JsdRyqx_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.JsdRyqx_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.JsdRyqx_Info model=new Maticsoft.Model.JsdRyqx_Info();
			if (row != null)
			{
				if(row["jsdbh"]!=null)
				{
					model.jsdbh=row["jsdbh"].ToString();
				}
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["kh"]!=null)
				{
					model.kh=row["kh"].ToString();
				}
				if(row["ryid"]!=null && row["ryid"].ToString()!="")
				{
					model.ryid=int.Parse(row["ryid"].ToString());
				}
				if(row["sfhbmd"]!=null && row["sfhbmd"].ToString()!="")
				{
					if((row["sfhbmd"].ToString()=="1")||(row["sfhbmd"].ToString().ToLower()=="true"))
					{
						model.sfhbmd=true;
					}
					else
					{
						model.sfhbmd=false;
					}
				}
				if(row["sfkqfq"]!=null && row["sfkqfq"].ToString()!="")
				{
					if((row["sfkqfq"].ToString()=="1")||(row["sfkqfq"].ToString().ToLower()=="true"))
					{
						model.sfkqfq=true;
					}
					else
					{
						model.sfkqfq=false;
					}
				}
				if(row["yxjcsj"]!=null && row["yxjcsj"].ToString()!="")
				{
					model.yxjcsj=DateTime.Parse(row["yxjcsj"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select jsdbh,sfzh,kh,ryid,sfhbmd,sfkqfq,yxjcsj ");
			strSql.Append(" FROM JsdRyqx_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" jsdbh,sfzh,kh,ryid,sfhbmd,sfkqfq,yxjcsj ");
			strSql.Append(" FROM JsdRyqx_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM JsdRyqx_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sfzh desc");
			}
			strSql.Append(")AS Row, T.*  from JsdRyqx_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "JsdRyqx_Info";
			parameters[1].Value = "sfzh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

