﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Sb_Info
	/// </summary>
	public partial class Sb_Info
	{
		public Sb_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("sblsh", "Sb_Info"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int sblsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Sb_Info");
			strSql.Append(" where sblsh=@sblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sblsh", SqlDbType.SmallInt)
			};
			parameters[0].Value = sblsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Maticsoft.Model.Sb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Sb_Info(");
			strSql.Append("sbbs,sbh,sbyt,sblx,kzlx,ljlx,comip,bddk,sjtbzt)");
			strSql.Append(" values (");
			strSql.Append("@sbbs,@sbh,@sbyt,@sblx,@kzlx,@ljlx,@comip,@bddk,@sjtbzt)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@sbbs", SqlDbType.VarChar,50),
					new SqlParameter("@sbh", SqlDbType.SmallInt,2),
					new SqlParameter("@sbyt", SqlDbType.SmallInt,2),
					new SqlParameter("@sblx", SqlDbType.SmallInt,2),
					new SqlParameter("@kzlx", SqlDbType.SmallInt,2),
					new SqlParameter("@ljlx", SqlDbType.SmallInt,2),
					new SqlParameter("@comip", SqlDbType.VarChar,30),
					new SqlParameter("@bddk", SqlDbType.VarChar,10),
					new SqlParameter("@sjtbzt", SqlDbType.SmallInt,2)};
			parameters[0].Value = model.sbbs;
			parameters[1].Value = model.sbh;
			parameters[2].Value = model.sbyt;
			parameters[3].Value = model.sblx;
			parameters[4].Value = model.kzlx;
			parameters[5].Value = model.ljlx;
			parameters[6].Value = model.comip;
			parameters[7].Value = model.bddk;
			parameters[8].Value = model.sjtbzt;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Sb_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Sb_Info set ");
			strSql.Append("sbbs=@sbbs,");
			strSql.Append("sbh=@sbh,");
			strSql.Append("sbyt=@sbyt,");
			strSql.Append("sblx=@sblx,");
			strSql.Append("kzlx=@kzlx,");
			strSql.Append("ljlx=@ljlx,");
			strSql.Append("comip=@comip,");
			strSql.Append("bddk=@bddk,");
			strSql.Append("sjtbzt=@sjtbzt");
			strSql.Append(" where sblsh=@sblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sbbs", SqlDbType.VarChar,50),
					new SqlParameter("@sbh", SqlDbType.SmallInt,2),
					new SqlParameter("@sbyt", SqlDbType.SmallInt,2),
					new SqlParameter("@sblx", SqlDbType.SmallInt,2),
					new SqlParameter("@kzlx", SqlDbType.SmallInt,2),
					new SqlParameter("@ljlx", SqlDbType.SmallInt,2),
					new SqlParameter("@comip", SqlDbType.VarChar,30),
					new SqlParameter("@bddk", SqlDbType.VarChar,10),
					new SqlParameter("@sjtbzt", SqlDbType.SmallInt,2),
					new SqlParameter("@sblsh", SqlDbType.SmallInt,2)};
			parameters[0].Value = model.sbbs;
			parameters[1].Value = model.sbh;
			parameters[2].Value = model.sbyt;
			parameters[3].Value = model.sblx;
			parameters[4].Value = model.kzlx;
			parameters[5].Value = model.ljlx;
			parameters[6].Value = model.comip;
			parameters[7].Value = model.bddk;
			parameters[8].Value = model.sjtbzt;
			parameters[9].Value = model.sblsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int sblsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sb_Info ");
			strSql.Append(" where sblsh=@sblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sblsh", SqlDbType.SmallInt)
			};
			parameters[0].Value = sblsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string sblshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Sb_Info ");
			strSql.Append(" where sblsh in ("+sblshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Sb_Info GetModel(int sblsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 sblsh,sbbs,sbh,sbyt,sblx,kzlx,ljlx,comip,bddk,sjtbzt from Sb_Info ");
			strSql.Append(" where sblsh=@sblsh");
			SqlParameter[] parameters = {
					new SqlParameter("@sblsh", SqlDbType.SmallInt)
			};
			parameters[0].Value = sblsh;

			Maticsoft.Model.Sb_Info model=new Maticsoft.Model.Sb_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Sb_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Sb_Info model=new Maticsoft.Model.Sb_Info();
			if (row != null)
			{
				if(row["sblsh"]!=null && row["sblsh"].ToString()!="")
				{
					model.sblsh=int.Parse(row["sblsh"].ToString());
				}
				if(row["sbbs"]!=null)
				{
					model.sbbs=row["sbbs"].ToString();
				}
				if(row["sbh"]!=null && row["sbh"].ToString()!="")
				{
					model.sbh=int.Parse(row["sbh"].ToString());
				}
				if(row["sbyt"]!=null && row["sbyt"].ToString()!="")
				{
					model.sbyt=int.Parse(row["sbyt"].ToString());
				}
				if(row["sblx"]!=null && row["sblx"].ToString()!="")
				{
					model.sblx=int.Parse(row["sblx"].ToString());
				}
				if(row["kzlx"]!=null && row["kzlx"].ToString()!="")
				{
					model.kzlx=int.Parse(row["kzlx"].ToString());
				}
				if(row["ljlx"]!=null && row["ljlx"].ToString()!="")
				{
					model.ljlx=int.Parse(row["ljlx"].ToString());
				}
				if(row["comip"]!=null)
				{
					model.comip=row["comip"].ToString();
				}
				if(row["bddk"]!=null)
				{
					model.bddk=row["bddk"].ToString();
				}
				if(row["sjtbzt"]!=null && row["sjtbzt"].ToString()!="")
				{
					model.sjtbzt=int.Parse(row["sjtbzt"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sblsh,sbbs,sbh,sbyt,sblx,kzlx,ljlx,comip,bddk,sjtbzt ");
			strSql.Append(" FROM Sb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" sblsh,sbbs,sbh,sbyt,sblx,kzlx,ljlx,comip,bddk,sjtbzt ");
			strSql.Append(" FROM Sb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Sb_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.sblsh desc");
			}
			strSql.Append(")AS Row, T.*  from Sb_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Sb_Info";
			parameters[1].Value = "sblsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

