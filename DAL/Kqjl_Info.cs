﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace Maticsoft.DAL
{
	/// <summary>
	/// 数据访问类:Kqjl_Info
	/// </summary>
	public partial class Kqjl_Info
	{
		public Kqjl_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long kqlsh)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Kqjl_Info");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(Maticsoft.Model.Kqjl_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Kqjl_Info(");
			strSql.Append("jsdsblsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff)");
			strSql.Append(" values (");
			strSql.Append("@jsdsblsh,@jsdm,@sfzh,@xm,@qybmm,@gz,@fbm,@kh,@kbs,@kqsj,@jczt,@sfdj,@kqlx,@yyxp,@zpxp,@sfff)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4),
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@gz", SqlDbType.VarChar,100),
					new SqlParameter("@fbm", SqlDbType.VarChar,100),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@kqsj", SqlDbType.DateTime),
					new SqlParameter("@jczt", SqlDbType.Bit,1),
					new SqlParameter("@sfdj", SqlDbType.Bit,1),
					new SqlParameter("@kqlx", SqlDbType.VarChar,50),
					new SqlParameter("@yyxp", SqlDbType.VarChar,100),
					new SqlParameter("@zpxp", SqlDbType.VarChar,100),
					new SqlParameter("@sfff", SqlDbType.Bit,1)};
			parameters[0].Value = model.jsdsblsh;
			parameters[1].Value = model.jsdm;
			parameters[2].Value = model.sfzh;
			parameters[3].Value = model.xm;
			parameters[4].Value = model.qybmm;
			parameters[5].Value = model.gz;
			parameters[6].Value = model.fbm;
			parameters[7].Value = model.kh;
			parameters[8].Value = model.kbs;
			parameters[9].Value = model.kqsj;
			parameters[10].Value = model.jczt;
			parameters[11].Value = model.sfdj;
			parameters[12].Value = model.kqlx;
			parameters[13].Value = model.yyxp;
			parameters[14].Value = model.zpxp;
			parameters[15].Value = model.sfff;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt64(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Maticsoft.Model.Kqjl_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Kqjl_Info set ");
			strSql.Append("jsdsblsh=@jsdsblsh,");
			strSql.Append("jsdm=@jsdm,");
			strSql.Append("sfzh=@sfzh,");
			strSql.Append("xm=@xm,");
			strSql.Append("qybmm=@qybmm,");
			strSql.Append("gz=@gz,");
			strSql.Append("fbm=@fbm,");
			strSql.Append("kh=@kh,");
			strSql.Append("kbs=@kbs,");
			strSql.Append("kqsj=@kqsj,");
			strSql.Append("jczt=@jczt,");
			strSql.Append("sfdj=@sfdj,");
			strSql.Append("kqlx=@kqlx,");
			strSql.Append("yyxp=@yyxp,");
			strSql.Append("zpxp=@zpxp,");
			strSql.Append("sfff=@sfff");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@jsdsblsh", SqlDbType.Int,4),
					new SqlParameter("@jsdm", SqlDbType.VarChar,50),
					new SqlParameter("@sfzh", SqlDbType.VarChar,18),
					new SqlParameter("@xm", SqlDbType.VarChar,10),
					new SqlParameter("@qybmm", SqlDbType.VarChar,100),
					new SqlParameter("@gz", SqlDbType.VarChar,100),
					new SqlParameter("@fbm", SqlDbType.VarChar,100),
					new SqlParameter("@kh", SqlDbType.VarChar,10),
					new SqlParameter("@kbs", SqlDbType.VarChar,20),
					new SqlParameter("@kqsj", SqlDbType.DateTime),
					new SqlParameter("@jczt", SqlDbType.Bit,1),
					new SqlParameter("@sfdj", SqlDbType.Bit,1),
					new SqlParameter("@kqlx", SqlDbType.VarChar,50),
					new SqlParameter("@yyxp", SqlDbType.VarChar,100),
					new SqlParameter("@zpxp", SqlDbType.VarChar,100),
					new SqlParameter("@sfff", SqlDbType.Bit,1),
					new SqlParameter("@kqlsh", SqlDbType.BigInt,8)};
			parameters[0].Value = model.jsdsblsh;
			parameters[1].Value = model.jsdm;
			parameters[2].Value = model.sfzh;
			parameters[3].Value = model.xm;
			parameters[4].Value = model.qybmm;
			parameters[5].Value = model.gz;
			parameters[6].Value = model.fbm;
			parameters[7].Value = model.kh;
			parameters[8].Value = model.kbs;
			parameters[9].Value = model.kqsj;
			parameters[10].Value = model.jczt;
			parameters[11].Value = model.sfdj;
			parameters[12].Value = model.kqlx;
			parameters[13].Value = model.yyxp;
			parameters[14].Value = model.zpxp;
			parameters[15].Value = model.sfff;
			parameters[16].Value = model.kqlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long kqlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Kqjl_Info ");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string kqlshlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Kqjl_Info ");
			strSql.Append(" where kqlsh in ("+kqlshlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Kqjl_Info GetModel(long kqlsh)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 kqlsh,jsdsblsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff from Kqjl_Info ");
			strSql.Append(" where kqlsh=@kqlsh");
			SqlParameter[] parameters = {
					new SqlParameter("@kqlsh", SqlDbType.BigInt)
			};
			parameters[0].Value = kqlsh;

			Maticsoft.Model.Kqjl_Info model=new Maticsoft.Model.Kqjl_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Maticsoft.Model.Kqjl_Info DataRowToModel(DataRow row)
		{
			Maticsoft.Model.Kqjl_Info model=new Maticsoft.Model.Kqjl_Info();
			if (row != null)
			{
				if(row["kqlsh"]!=null && row["kqlsh"].ToString()!="")
				{
					model.kqlsh=long.Parse(row["kqlsh"].ToString());
				}
				if(row["jsdsblsh"]!=null && row["jsdsblsh"].ToString()!="")
				{
					model.jsdsblsh=int.Parse(row["jsdsblsh"].ToString());
				}
				if(row["jsdm"]!=null)
				{
					model.jsdm=row["jsdm"].ToString();
				}
				if(row["sfzh"]!=null)
				{
					model.sfzh=row["sfzh"].ToString();
				}
				if(row["xm"]!=null)
				{
					model.xm=row["xm"].ToString();
				}
				if(row["qybmm"]!=null)
				{
					model.qybmm=row["qybmm"].ToString();
				}
				if(row["gz"]!=null)
				{
					model.gz=row["gz"].ToString();
				}
				if(row["fbm"]!=null)
				{
					model.fbm=row["fbm"].ToString();
				}
				if(row["kh"]!=null)
				{
					model.kh=row["kh"].ToString();
				}
				if(row["kbs"]!=null)
				{
					model.kbs=row["kbs"].ToString();
				}
				if(row["kqsj"]!=null && row["kqsj"].ToString()!="")
				{
					model.kqsj=DateTime.Parse(row["kqsj"].ToString());
				}
				if(row["jczt"]!=null && row["jczt"].ToString()!="")
				{
					if((row["jczt"].ToString()=="1")||(row["jczt"].ToString().ToLower()=="true"))
					{
						model.jczt=true;
					}
					else
					{
						model.jczt=false;
					}
				}
				if(row["sfdj"]!=null && row["sfdj"].ToString()!="")
				{
					if((row["sfdj"].ToString()=="1")||(row["sfdj"].ToString().ToLower()=="true"))
					{
						model.sfdj=true;
					}
					else
					{
						model.sfdj=false;
					}
				}
				if(row["kqlx"]!=null)
				{
					model.kqlx=row["kqlx"].ToString();
				}
				if(row["yyxp"]!=null)
				{
					model.yyxp=row["yyxp"].ToString();
				}
				if(row["zpxp"]!=null)
				{
					model.zpxp=row["zpxp"].ToString();
				}
				if(row["sfff"]!=null && row["sfff"].ToString()!="")
				{
					if((row["sfff"].ToString()=="1")||(row["sfff"].ToString().ToLower()=="true"))
					{
						model.sfff=true;
					}
					else
					{
						model.sfff=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select kqlsh,jsdsblsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff ");
			strSql.Append(" FROM Kqjl_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList2(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select a.sfzh,a.kqsj,a.xm,a.gz,a.jczt,dksj=CONVERT(varchar(10),a.kqsj,120),b.bzid,c.bzmc,d.gh from Kqjl_info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join Ry_Info (nolock) d on a.sfzh=d.sfzh ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList22(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select a.sfzh,a.kqsj,a.xm,a.gz,a.jczt,dksj=CONVERT(varchar(10),a.kqsj,120),bzid=0,bzmc=a.qybmm,gh='' from Kqjl_info(nolock) a ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 人员工资表信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListByPersonSalary(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xh=ROW_NUMBER()over(order by a.sfzh,b.kqsj), a.sfzh,a.gh,xb=case when a.xb=0 then '女' else '男' end,a.xm,gz=a.gz1,b.jczt,b.kqsj,rq=convert(varchar(10),b.kqsj,120),b.qybmm from Ry_Info a join Kqjl_Info b on a.sfzh=b.sfzh ");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 人员工资表信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListByPersonSalary2(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xh=ROW_NUMBER()over(order by a.sfzh,a.kqsj), a.sfzh,gh='',xb='',a.xm,gz='',a.jczt,a.kqsj,rq=convert(varchar(10),a.kqsj,120) from  Kqjl_Info a ");


            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 人员进出明细表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListByPersonPassInOut(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select a.gh,a.sfzh,a.xm,bz=b.qybmm,b.kh,b.kqsj,b.jczt from Ry_Info a join Kqjl_Info b on a.sfzh=b.sfzh ");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetListByEmployeeEntryExit(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("      with tb as (select * from Ryca_info a where lsh=(select top 1 lsh from RyCa_info where sfzh=a.sfzh order by fksj desc))"+
                          "select xh=ROW_NUMBER()over (order by c.bzid,a.xm ),a.sfzh,a.xm,xb=case when a.xb=0 then '女' else '男' end,ll=(YEAR(getdate())-YEAR(a.csrq)),a.zz,a.jg,a.mz,a.gh,a.csrq,gz=a.gz1,jtzz=a.zz,a.lxdh,a.jjllr,a.lldh,intime=fksj,outtime=cxsj,c.bzmc,aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='' from  Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join tb d on a.sfzh=d.sfzh");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetListByEmployeeEntryExit2(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("      with tb as (select * from Ryca_info a where lsh=(select top 1 lsh from RyCa_info where sfzh=a.sfzh order by fksj desc))" +
                          "select xh=ROW_NUMBER()over ( order by c.bzid,a.xm ),a.sfzh,a.xm,xb=case when a.xb=0 then '女' else '男' end,ll=(YEAR(getdate())-YEAR(a.csrq)),a.zz,a.jg,a.mz,a.gh,a.csrq,gz=a.gz1,jtzz=a.zz,a.lxdh,a.jjllr,a.lldh,intime=fksj,outtime=cxsj,c.bzmc,aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='' from  Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join tb d on a.sfzh=d.sfzh");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetListByEmployeeEntryExit3(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("   with ta as ( select a.sfzh,a.kqsj,a.jczt,b.xm,gz=b.gz1,a.qybmm  from Kqjl_Info a join Ry_Info b on a.sfzh=b.sfzh " + strWhere + " )" +
"select xh=ROW_NUMBER()over (order  by qybmm,xm ),sfzh,xm,gz," +
"max(case dd when 1 then jczt else 0 end) d1," +
"max(case dd when 2 then jczt else 0 end) d2," +
"max(case dd when 3 then jczt else 0 end) d3," +
"max(case dd when 4 then jczt else 0 end) d4," +
"max(case dd when 5 then jczt else 0 end) d5," +
"max(case dd when 6 then jczt else 0 end) d6," +
"max(case dd when 7 then jczt else 0 end) d7," +
"max(case dd when 8 then jczt else 0 end) d8," +
"max(case dd when 9 then jczt else 0 end) d9," +
"max(case dd when 10 then jczt else 0 end) d10," +
"max(case dd when 11 then jczt else 0 end) d11," +
"max(case dd when 12 then jczt else 0 end) d12," +
"max(case dd when 13 then jczt else 0 end) d13," +
"max(case dd when 14 then jczt else 0 end) d14," +
"max(case dd when 15 then jczt else 0 end) d15," +
"max(case dd when 16 then jczt else 0 end) d16," +
"max(case dd when 17 then jczt else 0 end) d17," +
"max(case dd when 18 then jczt else 0 end) d18," +
"max(case dd when 19 then jczt else 0 end) d19," +
"max(case dd when 20 then jczt else 0 end) d20," +
"max(case dd when 21 then jczt else 0 end) d21," +
"max(case dd when 22 then jczt else 0 end) d22," +
"max(case dd when 23 then jczt else 0 end) d23," +
"max(case dd when 24 then jczt else 0 end) d24," +
"max(case dd when 25 then jczt else 0 end) d25," +
"max(case dd when 26 then jczt else 0 end) d26," +
"max(case dd when 27 then jczt else 0 end) d27," +
"max(case dd when 28 then jczt else 0 end) d28," +
"max(case dd when 29 then jczt else 0 end) d29," +
"max(case dd when 30 then jczt else 0 end) d30," +
"max(case dd when 31 then jczt else 0 end) d31," +
"sum(jczt) totalCount " +
" from (select xm,gz,qybmm,sfzh,dd,jczt=1 from (select xm,gz,qybmm,sfzh,day(kqsj) as dd," +
" (select cast(jczt as varchar)+',' from ta where sfzh=a.sfzh and day(kqsj)=day(a.kqsj) " +
" order by kqsj  for xml path('') ) as zt from ta a group by sfzh,day(kqsj),xm,gz,qybmm) as tb " +
" where tb.zt like '%1,0%') as tc group by sfzh,xm,gz,qybmm order by qybmm,xm ;");
          
            return DbHelperSQL.Query(strSql.ToString(),300);
        }


        public DataSet GetListByEmployeeEntryExit4(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("       with ta as (select a.sfzh,a.kqsj,a.jczt,b.xm,gz=b.gz1,a.qybmm  from Kqjl_Info a join Ry_Info b on a.sfzh=b.sfzh " + strWhere + " )" +
 "   select xh=ROW_NUMBER()over (order  by qybmm,xm ),aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='',ii='',jj='',xm,gz,qybmm,sfzh,totalCount=COUNT(1) from ( select xm,gz,sfzh,day(kqsj) as dd,(select cast(jczt as varchar)+',' from ta where sfzh=a.sfzh and day(kqsj)=day(a.kqsj) order by kqsj  for xml path('') ) as zt,qybmm from ta a group by sfzh,day(kqsj),xm,gz,qybmm) as td where zt like '%1,0%' group by sfzh,xm,gz,qybmm order by qybmm,xm ");
           
            return DbHelperSQL.Query(strSql.ToString(),300);
        }

        public DataSet GetListByEmployeeEntryExit5(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("      with tb as (select * from Ryca_info a where lsh=(select top 1 lsh from RyCa_info where sfzh=a.sfzh order by fksj desc))" +
                          "select xh=ROW_NUMBER()over (order  by c.bzmc,d.fksj ),a.sfzh,a.xm,xb=case when a.xb=0 then '女' else '男' end,ll=(YEAR(getdate())-YEAR(a.csrq)),a.zz,a.jg,a.mz,a.gh,a.csrq,gz=a.gz1,jtzz=a.zz,a.lxdh,a.jjllr,a.lldh,intime=fksj,outtime=cxsj,c.bzmc,aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='' from  Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join tb d on a.sfzh=d.sfzh ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order  by c.bzmc,d.fksj ");
            return DbHelperSQL.Query(strSql.ToString(),300);
        }

        public DataSet GetOutPersonNum(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("     with tb as (select * from Ryca_info a where   lsh=(select top 1 lsh from RyCa_info where sfzh=a.sfzh order by fksj desc))" +
"select totalCount=count(1) from (select  a.sfzh,a.xm,xb=case when a.xb=0 then '女' else '男' end,ll=(YEAR(getdate())-YEAR(a.csrq)),a.zz,a.jg,a.mz,a.gh,a.csrq,gz=a.gz1,jtzz=a.zz,a.lxdh,a.jjllr,a.lldh,intime=fksj,outtime=cxsj,c.bzmc,aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='' from  Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join tb d on a.sfzh=d.sfzh "+strWhere +" ) as td;");
           
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetWorkPersonNum(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("     with tb as (select * from Ryca_info a where fksj is not null and cxsj is null and  lsh=(select top 1 lsh from RyCa_info where sfzh=a.sfzh order by fksj desc))" +
" select totalCount=count(1) from (select xh=ROW_NUMBER()over (order  by a.createtime desc ),a.sfzh,a.xm,xb=case when a.xb=0 then '女' else '男' end,ll=(YEAR(getdate())-YEAR(a.csrq)),a.zz,a.jg,a.mz,a.gh,a.csrq,gz=a.gz1,jtzz=a.zz,a.lxdh,a.jjllr,a.lldh,intime=fksj,outtime=cxsj,c.bzmc,aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='' from  Ry_Info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join tb d on a.sfzh=d.sfzh "+strWhere +" ) as td;");
         
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetListByEmployeeEntryExit6(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("      with ta as (select a.qybmm,a.sfzh,a.kqsj,a.jczt,b.xm,gz=b.gz1,c.fksj,c.cxsj from Kqjl_Info a join Ry_Info b on a.sfzh=b.sfzh join RyCa_Info c on a.sfzh=c.sfzh where c.cxsj is not null "+ strWhere +" )" +
           " select xh=ROW_NUMBER()over (order  by qybmm,cxsj),aa='',bb='',cc='',dd='',ee='',ff='',gg='',hh='',ii='',jj='',xm,gz,intime=fksj,outtime=cxsj,sfzh,totalCount=COUNT(1) from ( select xm,gz,qybmm,sfzh,day(kqsj) as dd,fksj,cxsj,(select cast(jczt as varchar)+',' from ta where sfzh=a.sfzh and day(kqsj)=day(a.kqsj) order by kqsj  for xml path('') ) as zt from ta a group by sfzh,day(kqsj),xm,gz,qybmm,fksj,cxsj) as td where zt like '%1,0%' group by sfzh,xm,gz,qybmm,fksj,cxsj order by qybmm,cxsj ");
          
            return DbHelperSQL.Query(strSql.ToString(),300);
        }


        public DataSet GetListByAttendanceStatistics(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("     select a.sfzh,a.kqsj,a.xm,a.gz,a.jczt,dksj=CONVERT(varchar(10),a.kqsj,120),b.bzid,c.bzmc,d.gh,xb=case when d.xb=0 then '女' else '男' end from Kqjl_info(nolock) a join Bzry_Info(nolock) b on a.sfzh=b.sfzh join Banz_Info(nolock) c on b.bzid=c.bzid join Ry_Info (nolock) d on a.sfzh=d.sfzh join Fenbry_Info(nolock) e on e.sfzh =a.sfzh join Fenb_Info(nolock) f on e.fbid=f.fbid ");  
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" kqlsh,jsdsblsh,jsdm,sfzh,xm,qybmm,gz,fbm,kh,kbs,kqsj,jczt,sfdj,kqlx,yyxp,zpxp,sfff ");
			strSql.Append(" FROM Kqjl_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Kqjl_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.kqlsh desc");
			}
			strSql.Append(")AS Row, T.*  from Kqjl_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Kqjl_Info";
			parameters[1].Value = "kqlsh";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

