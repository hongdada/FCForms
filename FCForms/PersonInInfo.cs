﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Maticsoft.DAL;
using System.Reflection;
using System.IO;
using Commons.CommonHelper;

namespace FCForms
{
    public partial class PersonInInfo : Form, IModuleExtension 
    {
        public PersonInInfo()
        {
            InitializeComponent();
            Init();
            bind();
        }

        private void Init()
        {
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ShowIcon = false;
            //表头加粗，居中
            foreach (DataGridViewColumn col in this.dataGridView1.Columns)
            {
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView1.AllowUserToAddRows = false;
            //班组
            Maticsoft.DAL.Banz_Info info = new Banz_Info();
            DataTable dt = info.GetList("").Tables[0];
            DataRow dr3 = dt.NewRow();
            dr3["bzid"] = 0;
            dr3["bzmc"] = "全部";
            dt.Rows.InsertAt(dr3, 0);
            this.cboTeam.DisplayMember = "bzmc";
            this.cboTeam.ValueMember = "bzid";
            this.cboTeam.DataSource = dt;
            //时间
            this.picBegin.Value = DateTime.Now.AddDays(-7);
            this.picEnd.Value = DateTime.Now;
        }

        private DataTable bind()
        {
            Maticsoft.DAL.Kqjl_Info info = new Kqjl_Info();
            string sql = string.Empty;
            sql = " 1=1 ";
            string team = this.cboTeam.Text.ToString().Trim();
            if (team != "全部")
            {
                sql += " and c.bzmc='" + team + "'";
            }
            DateTime beginT = Convert.ToDateTime(picBegin.Value);
            DateTime endT = Convert.ToDateTime(picEnd.Value);
            if (beginT > endT)
            {
                MessageBox.Show("开始时间不能大于结束时间！"); return new DataTable();
            }
            sql += " and CONVERT(varchar(10),d.fksj,120)<='" + endT.ToString("yyyy-MM-dd") + "' and CONVERT(varchar(10),d.fksj,120)>='" + beginT.ToString("yyyy-MM-dd") + "'";
            DataTable dt = info.GetListByEmployeeEntryExit5(sql).Tables[0];
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.DataSource = dt;
            return dt;
        }

        /// <summary>
        /// 获取这段时间内退场人数
        /// </summary>
        /// <returns></returns>
        private int GetOutPersonNum()
        {
            Maticsoft.DAL.Kqjl_Info info = new Kqjl_Info();
            string sql = string.Empty;
            sql = " where 1=1 ";
            string team = this.cboTeam.Text.ToString().Trim();
            if (team != "全部")
            {
                sql += " and c.bzmc='" + team + "'";
            }
            DateTime beginT = Convert.ToDateTime(picBegin.Value);
            DateTime endT = Convert.ToDateTime(picEnd.Value);

            sql += " and CONVERT(varchar(10),d.cxsj,120)<='" + endT.ToString("yyyy-MM-dd") + "' and CONVERT(varchar(10),d.cxsj,120)>='" + beginT.ToString("yyyy-MM-dd") + "'";
            DataTable dt = info.GetOutPersonNum(sql).Tables[0];
            if (dt.Rows.Count == 0) { return 0; }
            else { int num = Convert.ToInt32(dt.Rows[0][0]); return num; }
        }

        /// <summary>
        /// 获取这段时间内工作过的人员数量
        /// </summary>
        /// <returns></returns>
        private int GetWorkPersonNum()
        {
            Maticsoft.DAL.Kqjl_Info info = new Kqjl_Info();
            string sql = string.Empty;
            sql = " where  1=1 ";
            string team = this.cboTeam.Text.ToString().Trim();
            if (team != "全部")
            {
                sql += " and c.bzmc='" + team + "'";
            }
            DateTime beginT = Convert.ToDateTime(picBegin.Value);
            DateTime endT = Convert.ToDateTime(picEnd.Value);

            sql += " and CONVERT(varchar(10),d.fksj,120)<='" + endT.ToString("yyyy-MM-dd") + "'";
            DataTable dt = info.GetWorkPersonNum(sql).Tables[0];
            if (dt.Rows.Count == 0) { return 0; }
            else { int num = Convert.ToInt32(dt.Rows[0][0]); return num; }
        }


        /// <summary>
        /// 调用窗口
        /// </summary>
        /// <param name="title">窗体名称</param>
        /// <param name="xmmc">项目名称</param>
        /// <returns></returns>
        public bool ShowExtension(string title, string xmmc)
        {
            try
            {
                PersonInInfo f = new PersonInInfo();
                f.ShowDialog();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("加载失败！");
                return false;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            bind();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = bind();
                int outNum = GetOutPersonNum();
                int workNum = GetWorkPersonNum();
                int inNum = dt.Rows.Count;
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("没有数据！"); return;
                }
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                //设置文件类型   
                saveFileDialog1.Filter = " xls files(*.xls)|*.xls";
                //设置默认文件类型显示顺序   
                saveFileDialog1.FilterIndex = 2;
                //保存对话框是否记忆上次打开的目录   
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.FileName = "本周进场务工人员情况表" + DateTime.Now.ToString("yyyyMMdd");
                //点了保存按钮进入   
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string team = this.cboTeam.Text.ToString().Trim();
                    string fileName = saveFileDialog1.FileName;
                    string[] oldCols = new string[] { "xh", "xm", "sfzh", "intime","gz", "aa", "bb", "cc" };
                    string[] newCols = new string[] { "编号", "本周进场农民工姓名", "身份证号", "进场时间", "工种", "是否在市建委备案", "是否签订《劳动合同》", "备注" };
                    ExcelHelper.ExportByEmployeeEntryExit5(dt, "本周进场务工人员情况表", fileName, "Sheet1", oldCols, newCols, team + "," + this.picBegin.Value.ToString("yyyy年MM月dd日") + "," + this.picEnd.Value.ToString("yyyy年MM月dd日") + "," + inNum + "," + outNum + "," + workNum);
                    MessageBox.Show("导出文件成功！"); return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出文件故障！"); return;
            }
        }
    }
}
