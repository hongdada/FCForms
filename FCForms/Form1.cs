﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FCForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EmployeeEntryExit f = new EmployeeEntryExit();
            f.ShowExtension("", "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NewEmployeeEntryExit f = new NewEmployeeEntryExit();
            f.ShowExtension("", "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AttendanceStatistics f = new AttendanceStatistics();
            f.ShowExtension("", "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PersonSalary f = new PersonSalary();
            f.ShowExtension("", "");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PersonInInfo f = new PersonInInfo();
            f.ShowExtension("", "");
        }
        private void button6_Click(object sender, EventArgs e)
        {
            PersonOutInfo f = new PersonOutInfo();
            f.ShowExtension("", "");
        }
    }
}
