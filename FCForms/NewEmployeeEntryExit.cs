﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Maticsoft.DAL;
using System.Reflection;
using System.IO;
using Commons.CommonHelper;

namespace FCForms 
{
    public partial class NewEmployeeEntryExit : Form,IModuleExtension 
    {
        public NewEmployeeEntryExit()
        {
            InitializeComponent();
            Init();
            bind();
        }
        private void Init()
        {
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ShowIcon = false;
            //表头加粗，居中
            foreach (DataGridViewColumn col in this.dataGridView1.Columns)
            {
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView1.AllowUserToAddRows = false;
            //班组
            Maticsoft.DAL.Banz_Info info = new Banz_Info();
            DataTable dt = info.GetList("").Tables[0];
            List<string> strList = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strList.Add(dt.Rows[i]["bzmc"].ToString().Trim());
            }
            List<string> list = new List<string>();
            list.Add("全部");
            list.AddRange(strList);
            this.cboTeam.DataSource = list;
            //年，月
            int[] months = new int[12] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            this.cboMonth.DataSource = months;
            this.cboMonth.Text = DateTime.Now.Month.ToString();
            int year = DateTime.Now.Year;
            int[] years = new int[12] { year - 11, year - 10, year - 9, year - 8, year - 7, year - 6, year - 5, year - 4, year - 3, year - 2, year - 1, year };
            this.cboYear.DataSource = years;
            this.cboYear.Text = DateTime.Now.Year.ToString();
        }

        private DataTable bind()
        {
            Maticsoft.DAL.Kqjl_Info info = new Kqjl_Info();
            string sql = string.Empty;
            sql = " 1=1 ";
            string team = this.cboTeam.Text.ToString().Trim();
            if (team != "全部")
            {
                sql += " and c.bzmc='" + team + "'";
            }
            int year = Convert.ToInt32(this.cboYear.Text.ToString());
            int month = Convert.ToInt32(this.cboMonth.Text.ToString());
            string strT = year + "/" + month + "/" + "01";
            string time = Convert.ToDateTime(strT).AddMonths(1).AddMilliseconds(-1).ToString("yyyy-MM-dd HH:mm:ss:fff");
            sql += " and d.fksj<='" + time + "'";
            sql += " order by c.bzid,a.xm  ";
            DataTable dt = info.GetListByEmployeeEntryExit2(sql).Tables[0];
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.DataSource = dt;
            return dt;
        }
 

        /// <summary>
        /// 调用窗口
        /// </summary>
        /// <param name="title">窗体名称</param>
        /// <param name="xmmc">项目名称</param>
        /// <returns></returns>
        public bool ShowExtension(string title, string xmmc)
        {
            try
            {
                NewEmployeeEntryExit f = new NewEmployeeEntryExit();
                f.ShowDialog();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("加载失败！");
                return false;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            bind();
        }

        private void btnExport_Click_1(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = bind();
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("没有数据！"); return;
                }
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                //设置文件类型   
                saveFileDialog1.Filter = " xls files(*.xls)|*.xls";
                //设置默认文件类型显示顺序   
                saveFileDialog1.FilterIndex = 2;
                //保存对话框是否记忆上次打开的目录   
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.FileName = "人员花名册" + DateTime.Now.ToString("yyyyMMdd");
                //点了保存按钮进入   
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string team = this.cboTeam.Text.ToString().Trim();
                    string fileName = saveFileDialog1.FileName;
                    string[] oldCols = new string[] { "xh", "xm", "xb", "gz", "jtzz", "sfzh", "dd" };
                    string[] newCols = new string[] { "编号", "姓名", "性别", "工种", "家庭住址", "身份证号", "劳动合同编号" };
                    ExcelHelper.ExportByEmployeeEntryExit2(dt, "人员花名册", fileName, "Sheet1", oldCols, newCols, team + "," + this.cboYear.Text.ToString() + "," + this.cboMonth.Text.ToString());
                    MessageBox.Show("导出文件成功！"); return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出文件故障！"); return;
            }
        }
    }
}
